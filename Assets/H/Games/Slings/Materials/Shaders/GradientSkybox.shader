﻿Shader "Huuuge/ZenBox/GradientSkybox"
{
    Properties
    {
        _Color1 ("Color Down", Color) = (1, 1, 1, 0)
        _Color2 ("Color Up", Color) = (1, 1, 1, 0)
        _Stipes ("Stripes", int) = 100
    }

    CGINCLUDE

    #include "UnityCG.cginc"

    struct appdata
    {
        float4 position : POSITION;
        float3 texcoord : TEXCOORD0;
    };
    
    struct v2f
    {
        float4 position : SV_POSITION;
        float3 texcoord : TEXCOORD0;
    };
    
    half4 _Color1;
    half4 _Color2;
    int _Stipes; 
    
    v2f vert (appdata v)
    {
        v2f o;
        o.position = UnityObjectToClipPos (v.position);
        o.texcoord = v.texcoord;
        return o;
    }
    
    fixed4 frag (v2f i) : COLOR
    {
        fixed4 c = lerp (_Color1, _Color2, i.texcoord.y * 0.5f + 0.5f);
        int x = int(floor(i.texcoord.x * _Stipes));
			if (fmod(x, 2) == 0){
			    c.rgb.x *= 0.95;
			    c.rgb.y *= 0.95;
			    c.rgb.z *= 0.95;
	        }
        return c;
    }

    ENDCG

    SubShader
    {
        Tags { "RenderType"="Background" "Queue"="Background" }
        Pass
        {
            ZWrite Off
            Cull Off
            Fog { Mode Off }
            CGPROGRAM
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma vertex vert
            #pragma fragment frag
            ENDCG
        }
    }
}