﻿Shader "Custom/ScreenGradient"
{
	Properties
	{
		_BottomColor("Bottom Color", Color) = (1,1,1,1)
		_TopColor("Top Color", Color) = (1,1,1,1)
		_Offset("Offset", float) = 0
	}

	SubShader
	{
		Tags
		{
			"Queue" = "Geometry"
			"IgnoreProjector" = "True"
			"RenderType" = "Opaque"
			"PreviewType" = "Plane"
			"LightMode" = "Always"
		}

		Cull Off
		Lighting Off

		Pass
		{
		CGPROGRAM

		#pragma vertex vert
		#pragma fragment frag

		struct appdata_t
		{
			float4 position	: POSITION;
			float2 uv : TEXCOORD0;
		};

		struct v2f
		{                 
			float4 position	: SV_POSITION;
			half4 color		: COLOR;
			float2 uv : TEXCOORD0;
		};

		half4 _BottomColor;
		half4 _TopColor;
		float _Offset;

		v2f vert(appdata_t IN)
		{
			v2f OUT;
			OUT.position = UnityObjectToClipPos(IN.position);

			float factor = OUT.position.y*-0.5+0.5;
			factor *= 1 + _Offset*2;
			factor -= _Offset;
			factor = clamp(factor, 0, 1);
			OUT.color = lerp(_TopColor, _BottomColor, factor);
            OUT.uv = IN.uv;
			return OUT;
		}

		half4 frag(v2f IN) : SV_Target
		{
			half4 c;
			c.rgb = IN.color.rgb;
			c.a = 1;
			
			int x = int(floor(IN.uv.x * 100));
			if (fmod(x, 2) == 0){
			    c.rgb.x *= 0.95;
			    c.rgb.y *= 0.95;
			    c.rgb.z *= 0.95;
	        }
			return c;
		}

		ENDCG
		}
	}
}