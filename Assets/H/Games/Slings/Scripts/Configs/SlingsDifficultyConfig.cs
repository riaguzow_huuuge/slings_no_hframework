﻿using System;
using System.Collections.Generic;
using H.Games.Slings.Utilities;
using HFramework.Config;
using UnityEngine;

namespace H.Games.Slings.Configs
{
    [CreateAssetMenu(fileName = "SlingsDifficultyConfig.asset", menuName = "Slings/SlingsDifficultyConfig")]
    [Serializable]
    public class SlingsDifficultyConfig : AbstractConfig
    {
        [Header("Baskets")]
        [SerializeField]
        MinMax verticalSpawnRange;

        [SerializeField]
        float horizontalBasketsSpeedFactor;

        [SerializeField]
        float verticalBasketsSpeedFactor;

        [SerializeField]
        List<MovingBoxSpeedTier> speedTiers = new List<MovingBoxSpeedTier>();

        [Space, Header("Trajectory")]
        [SerializeField]
        int trajectoryDotsCount;

        [SerializeField]
        float trajectoryDotsStep;

        [SerializeField]
        float trajectoryOriginStep;

        [Header("Stars")]
        [SerializeField]
        StarChances starChances;

        public MinMax VerticalSpawnRange
        {
            get { return verticalSpawnRange; }
        }

        public float HorizontalBasketsSpeedFactor
        {
            get { return horizontalBasketsSpeedFactor; }
        }

        public float VerticalBasketsSpeedFactor
        {
            get { return verticalBasketsSpeedFactor; }
        }

        public List<MovingBoxSpeedTier> SpeedTiers
        {
            get { return speedTiers; }
        }

        public int TrajectoryDotsCount
        {
            get { return trajectoryDotsCount; }
        }

        public float TrajectoryDotsStep
        {
            get { return trajectoryDotsStep; }
        }

        public float TrajectoryOriginStep
        {
            get { return trajectoryOriginStep; }
        }

        public StarChances StarBoxChances
        {
            get { return starChances; }
        }
    }

    [Serializable]
    public class StarChances
    {
        [SerializeField]
        float starBoxChance;

        [SerializeField]
        float starChance;

        [SerializeField]
        float starPathChance;

        [SerializeField]
        float noStarsChance;

        public float StarBoxChance
        {
            get { return starBoxChance; }
        }

        public float StarChance
        {
            get { return starChance; }
        }

        public float StarPathChance
        {
            get { return starPathChance; }
        }

        public float NoStarsChance
        {
            get { return noStarsChance; }
        }
    }

    [Serializable]
    public class MovingBoxSpeedTier
    {
        [SerializeField]
        MinMax pointsRange;

        [SerializeField]
        float speedFactor;

        public MinMax PointsRange
        {
            get { return pointsRange; }
        }

        public float SpeedFactor
        {
            get { return speedFactor; }
        }
    }
}