﻿using System.Collections.Generic;
using UnityEngine;

namespace H.Games.Slings.Configs
{
    [CreateAssetMenu(fileName = "SlingsGameSave.asset", menuName = "Slings/SlingsGameSave")]
    [System.Serializable]
    public class SlingsSaveData : ScriptableObject
    {
        const string BACKGROUNDS_CATEGORY = "Backgrounds";
        const string BASKETS_CATEGORY = "Baskets";
        const string SKIN_CATEGORY = "Skins";
        [SerializeField] string currentBackground = "1";
        [SerializeField] string currentBasket = "1";
        [SerializeField] string currentSkin = "1";

        public string BackgroundsCategory
        {
            get { return BACKGROUNDS_CATEGORY; }
        }
        
        public string BasketsCategory
        {
            get { return BASKETS_CATEGORY; }
        }

        public string SkinCategory
        {
            get { return SKIN_CATEGORY; }
        }

        public string GetCurrentBasket
        {
            get { return currentBasket; }
        }
        
        public string GetCurrentBackground
        {
            get { return currentBackground; }
        }

        public string GetCurrentSkin
        {
            get { return currentSkin; }
        }
        
        public void SetCurrentBackground(string itemId)
        {
            currentBackground = itemId;
        }
        
        public void SetCurrentBasket(string itemId)
        {
            currentBasket = itemId;
        }
        
        public void SetCurrentSkin(string itemId)
        {
            currentSkin = itemId;
        }

        public Dictionary<string, List<string>> GetSelectedVanityItems()
        {
            var selectedItems = new Dictionary<string, List<string>>()
            {
                { BACKGROUNDS_CATEGORY, new List<string>() { currentBackground } },
                { BASKETS_CATEGORY, new List<string>() { currentBasket } },
                { SKIN_CATEGORY, new List<string>() { currentSkin } }
            };
            return selectedItems;
        }
    }
}