﻿using H.Games.Slings.CoreModules;
using UnityEngine;
using UnityEngine.UI;

namespace H.Games.Slings.Development
{
    public class CheatButton : MonoBehaviour
    {
        void Awake()
        {
#if DEVELOPMENT_BUILD
            GetComponent<Button>().onClick.AddListener(OnClickAction);
#else
      Destroy(gameObject);  
#endif
        }

        void OnClickAction()
        {
#if DEVELOPMENT_BUILD
            Core.Instance.CheatScore();
#endif
        }

    }
}