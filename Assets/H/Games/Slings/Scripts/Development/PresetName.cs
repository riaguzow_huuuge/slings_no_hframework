﻿using UnityEngine;
using UnityEngine.UI;

namespace H.Games.Slings.Development
{
    public class PresetName : MonoBehaviour
    {
        Text t;
        
        void Awake()
        {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            t = GetComponent<Text>();
#else
            DestroyMe();
#endif
        }

        public void SetPresetName(string name)
        {
            t.text = name;
        }

        public void DestroyMe()
        {
            Destroy(gameObject);
        }
    }
}