﻿using Games.Slings.CoreModules;
using H.Games.Slings.CoreModules;
using HFramework.API;
using HFramework.Development;
using UnityEngine;

namespace H.Games.Slings.Development
{
    public class SlingsDeveloperConsole : MonoBehaviour
    {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        const string HIDE_PRESET_NAMES = "Hide preset names";
        const string STOP_LOOP = "Stop Loop";
        const string START_LOOP = "Start Loop";
        const string CHEAT_SCORING = "Cheat Scoring";

        static IDevPanel DevPanel
        {
            get { return HApi.DevPanel; }
        }
        
        void Start()
        {
            if (DevPanel == null)
                return;
            
//            devPanel.AddCustomAction(">>>RELOAD<<<", HCommon.Scenes.LoadStartupScene);
            DevPanel.AddCustomAction(CHEAT_SCORING, ChangeCheats);
            DevPanel.AddCustomAction(START_LOOP, LoopStart);
            DevPanel.AddCustomAction(STOP_LOOP, LoopEnd);
            DevPanel.AddCustomAction(HIDE_PRESET_NAMES, HidePresetNames);
        }

        static void ChangeCheats()
        {
            var ic = FindObjectOfType<InputController>();
            ic.isCheatMode = !ic.isCheatMode;
        }

        static void LoopStart()
        {
            Core.Instance.StartCheatScoring();
        }

        static void LoopEnd()
        {
            Core.Instance.StopCheatScoring();
        }

        void HidePresetNames()
        {
            var presetName = FindObjectOfType<PresetName>();
            if (presetName != null)
                presetName.DestroyMe();
        }

        void OnDestroy()
        {
            if (DevPanel == null)
                return;
            
            DevPanel.RemoveCustomAction(CHEAT_SCORING);
            DevPanel.RemoveCustomAction(START_LOOP);
            DevPanel.RemoveCustomAction(STOP_LOOP);
            DevPanel.RemoveCustomAction(HIDE_PRESET_NAMES);

        }
#endif
    }
}