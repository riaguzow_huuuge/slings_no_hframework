﻿using System.Collections.Generic;
using Anima2D;
using H.Games.Slings.CoreModules;
using UnityEngine;

namespace H.Games.Slings.Customisation
{
    public class SkinCustomisation : MonoBehaviour
    {
        [SerializeField]
        List<SpriteMeshAnimation> spriteMeshes;

        void OnEnable()
        {
            UpdateSkin();
        }

        void UpdateSkin()
        {
            var currentSkinIdx = Core.Customisation.GetCurrentSkinIndex();
            SetSkin(currentSkinIdx);
        }

        void SetSkin(int idx)
        {
            for (int i = 0; i < spriteMeshes.Count; i++)
                spriteMeshes[i].frame = idx;
        }
    }
}