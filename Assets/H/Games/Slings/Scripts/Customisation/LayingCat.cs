﻿using H.Games.Slings.CoreModules;
using UnityEngine;

namespace H.Games.Slings.Customisation
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class LayingCat : MonoBehaviour
    {
        [SerializeField]
        SpriteRenderer spriteRenderer;

        void OnEnable()
        {
            GamestateController.Instance.OnSkinChanged += UpdateSkin;
        }

        void OnDisable()
        {
            if (GamestateController.Instance != null)
            {
                GamestateController.Instance.OnSkinChanged -= UpdateSkin;
            }
        }
        
        void Awake()
        {
            UpdateSkin();
        }

        void UpdateSkin()
        {
            var skin = Core.Customisation.GetCurrentLayingCatSprite();
            spriteRenderer.sprite = skin;
        }

        public void Activate()
        {
            spriteRenderer.enabled = true;
        }

        public void Deactivate()
        {
            spriteRenderer.enabled = false;
        }
    }
}