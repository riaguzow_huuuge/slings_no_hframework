﻿using System.Collections.Generic;
using H.Games.Slings.CoreModules;
using HFramework.API;
using SubjectNerd.Utilities;
using UnityEngine;

namespace H.Games.Slings.Customisation
{
    [CreateAssetMenu(fileName = "SlingsCustomisation.asset", menuName = "Slings/SlingsCustomisation")]
    public class SlingsCustomisation : ScriptableObject
    {
        [SerializeField, Reorderable("Background")]
        List<BackgroundCustomisationData> backgrounds;
	
        [SerializeField, Reorderable("Baskets")]
        List<BasketCustomisationData> baskets;
        
        [SerializeField, Reorderable("Cat")]
        List<Sprite> layingCat;
        
        BackgroundCustomisationData GetBackground(int idx)
        {
            return backgrounds[idx];
        }
	
        BasketCustomisationData GetBasket(int idx)
        {
            return baskets[idx];
        }

        int GetItemId(string itemId)
        {
            var item = 0;
            if (int.TryParse(itemId, out item))
            {
                return item - 1;
            }

            return item;
        }

        public BasketCustomisationData GetCurrentBasketData()
        {
            var itemId = HApi.GetActiveCustomizationItemID(GamestateController.BASKETS_CATEGORY);
            return GetBasket(GetItemId(itemId));
        }
    
        public BackgroundCustomisationData GetCurrentBackgroundData()
        {
            var itemId = HApi.GetActiveCustomizationItemID(GamestateController.BACKGROUNDS_CATEGORY);
            return GetBackground(GetItemId(itemId));
        }

        public Sprite GetCurrentLayingCatSprite()
        {
            return layingCat[GetCurrentSkinIndex()];
        }
        
        public int GetCurrentSkinIndex()
        {
            var itemId = HApi.GetActiveCustomizationItemID(GamestateController.CATS_CATEGORY);
            return GetItemId(itemId);
        }
    }
}