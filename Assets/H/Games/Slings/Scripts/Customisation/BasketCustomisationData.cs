﻿using System;
using UnityEngine;

namespace H.Games.Slings.Customisation
{
    [Serializable]
    public class BasketCustomisationData
    {
        [SerializeField]
        Sprite topSprite;
	
        [SerializeField]
        Sprite frontSprite;
	
        public Sprite FrontSprite
        {
            get { return frontSprite; }
        }

        public Sprite TopSprite
        {
            get { return topSprite; }
        }
    }
}