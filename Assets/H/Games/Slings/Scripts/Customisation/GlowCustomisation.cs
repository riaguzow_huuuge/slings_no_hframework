﻿using H.Games.Slings.CoreModules;
using UnityEngine;

namespace H.Games.Slings.Customisation
{
    public class GlowCustomisation : MonoBehaviour
    {
        [SerializeField]
        SpriteRenderer glow;

        void OnEnable()
        {
            GamestateController.Instance.OnBackgroundChanged += UpdateGlow;
        }

        void OnDisable()
        {
            if (GamestateController.Instance != null)
            {
                GamestateController.Instance.OnBackgroundChanged -= UpdateGlow;
            }
        }

        void Awake()
        {
            UpdateGlow();
        }

        void UpdateGlow()
        {
            var bgData = Core.Customisation.GetCurrentBackgroundData();
            glow.color = bgData.GlowColor;
        }
    }
}