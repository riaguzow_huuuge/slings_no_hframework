﻿using H.Games.Slings.CoreModules;
using UnityEngine;

namespace H.Games.Slings.Customisation
{
    public class FrontBoxCustomisation : MonoBehaviour
    {
        [SerializeField]
        SpriteRenderer frontBox;

        void OnEnable()
        {
            GamestateController.Instance.OnBasketChanged += UpdateBasket;
        }

        void OnDisable()
        {
            if (GamestateController.Instance != null)
            {
                GamestateController.Instance.OnBasketChanged -= UpdateBasket;
            }
        }

        void Awake()
        {
            UpdateBasket();
        }

        void UpdateBasket()
        {
            var basketData = Core.Customisation.GetCurrentBasketData();
            frontBox.sprite = basketData.FrontSprite;
        }
    }
}