﻿using H.Games.Slings.CoreModules;
using UnityEngine;

namespace H.Games.Slings.Customisation
{
    public class TopBoxCustomisation : MonoBehaviour
    {
        [SerializeField]
        SpriteRenderer topBoxSprite;

        void OnEnable()
        {
            GamestateController.Instance.OnBasketChanged += UpdateBasket;
        }

        void OnDisable()
        {
            if (GamestateController.Instance != null)
            {
                GamestateController.Instance.OnBasketChanged -= UpdateBasket;
            }
        }

        void Awake()
        {
            UpdateBasket();
        }

        void UpdateBasket()
        {
            var bgData = Core.Customisation.GetCurrentBasketData();
            topBoxSprite.sprite = bgData.TopSprite;
        }
    }
}