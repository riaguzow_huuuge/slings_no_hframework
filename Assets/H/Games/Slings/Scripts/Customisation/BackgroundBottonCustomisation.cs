﻿using H.Games.Slings.CoreModules;
using UnityEngine;

namespace H.Games.Slings.Customisation
{
    public class BackgroundBottonCustomisation : MonoBehaviour
    {
        [SerializeField]
        SpriteRenderer backgroudBotton;

        void OnEnable()
        {
            GamestateController.Instance.OnBackgroundChanged += UpdateBackground;
        }

        void OnDisable()
        {
            if (GamestateController.Instance != null)
            {
                GamestateController.Instance.OnBackgroundChanged -= UpdateBackground;
            }
        }
        
        void Awake()
        {
            UpdateBackground();
        }

        void UpdateBackground()
        {
            var bgData = Core.Customisation.GetCurrentBackgroundData();
            backgroudBotton.sprite = bgData.BottomSprite;
        }
    }
}