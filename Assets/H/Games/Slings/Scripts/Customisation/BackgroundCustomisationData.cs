﻿using System;
using UnityEngine;

namespace H.Games.Slings.Customisation
{
    [Serializable]
    public class BackgroundCustomisationData
    {
        [SerializeField]
        Sprite bottomSprite;
	
        [SerializeField]
        Sprite tileSprite;
    
        [SerializeField]
        Color glowColor = Color.white;

        public Sprite TileSprite
        {
            get { return tileSprite; }
        }

        public Sprite BottomSprite
        {
            get { return bottomSprite; }
        }
    
        public Color GlowColor
        {
            get { return glowColor; }
        }
    }
}