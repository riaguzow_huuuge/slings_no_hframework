﻿using H.Games.Slings.CoreModules;
using UnityEngine;

namespace H.Games.Slings.Customisation
{
    public class BackgroundTileCustomosation : MonoBehaviour
    {
        [SerializeField]
        SpriteRenderer backgroundTile;

        void OnEnable()
        {
            GamestateController.Instance.OnBackgroundChanged += UpdateBackground;
        }

        void OnDisable()
        {
            if (GamestateController.Instance != null)
            {
                GamestateController.Instance.OnBackgroundChanged -= UpdateBackground;
            }
        }
        
        void Awake()
        {
            UpdateBackground();
        }

        void UpdateBackground()
        {
            var bgData = Core.Customisation.GetCurrentBackgroundData();
            backgroundTile.sprite = bgData.TileSprite;
        }
    }
}