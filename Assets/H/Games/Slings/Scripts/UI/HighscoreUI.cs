﻿using H.Games.Slings.CoreModules;
using UnityEngine;

namespace H.Games.Slings.UI
{
	public class HighscoreUI : MonoBehaviour
	{
		[SerializeField]
		GameObject highscoreText;
	

		void OnEnable()
		{
			Core.ScoreController.OnHighscoreBeatenEvent.AddListener(ShowHighscoreText);
		}

		void RemoveListener()
		{
			Core.ScoreController.OnHighscoreBeatenEvent.RemoveListener(ShowHighscoreText);
		}

		void Start()
		{
			highscoreText.SetActive(false);
		}

		void ShowHighscoreText()
		{
			highscoreText.SetActive(true);
			RemoveListener();
		}
	}
}
