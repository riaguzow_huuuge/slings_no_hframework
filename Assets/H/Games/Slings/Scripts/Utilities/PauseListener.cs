﻿using HFramework.API;
using H.Games.Slings.CoreModules;
using UnityEngine;
using UnityEngine.Events;

namespace H.Games.Slings.Utilities
{
    public class PauseListener : MonoBehaviour
    {
        UnityAction<bool> onPauseAction;

        void OnEnable()
        {
            HApi.OnGamePause += OnPause;
            HApi.OnGameContinue += OnContinue;
        }

        public void AddPauseAction(UnityAction<bool> action)
        {
            onPauseAction += action;
        }

        void OnContinue()
        {
            OnPause(false);
        }

        void OnPause(bool isPaused)
        {
            if (onPauseAction != null && Core.Instance.isGameplayStarted)
            {
                onPauseAction(isPaused);
            }
        }

        bool isQuitting = false;
        void OnApplicationQuit()
        {
            isQuitting = true;
        }
        void OnDisable()
        {
            if (!isQuitting)
            {
                StopListening();
            }
        }

        void StopListening()
        {
            HApi.OnGamePause -= OnPause;
            HApi.OnGameContinue -= OnContinue;
        }
    }
}

