﻿using UnityEngine;

namespace H.Games.Slings.Utilities
{
    public class SlingsSingleton<T> : MonoBehaviour where T : Component
    {
        static T mInstance;

        public static T Instance
        {
            get
            {
                {
                    if (mInstance != null)
                        return mInstance;

                    T[] t = FindObjectsOfType<T>();

                    if (t.Length == 0)
                    {
                        Debug.Log("Cant find object in scene");
                        return null;
                    }
                    if (t.Length == 1)
                    {
                        mInstance = t[0];
                        return mInstance;
                    }
                    var firstObject = t[0];
                    Debug.LogError("More than 1 instance of singleton class: " +
                                   firstObject.GetComponent<T>().GetType().ToString());
                    return firstObject;
                }
            }
        }
    }
}