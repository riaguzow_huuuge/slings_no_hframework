﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace H.Games.Slings.Utilities
{
    public static class SlingsUtils
    {
        public static T GetOrAddComponent<T>(this GameObject go) where T : Component
        {
            T pauseListener = go.GetComponent<T>();
            if (pauseListener == null)
                pauseListener = go.AddComponent<T>();
            return pauseListener;
        }

        public static Color SetAlpha(this Color color, float alpha)
        {
            return new Color(
                color.r,
                color.g,
                color.b,
                alpha
            );
        }

        public static Vector3 SetX(this Vector3 v, float x)
        {
            v.Set(x, v.y, v.z);
            return v;
        }

        public static Vector3 SetY(this Vector3 v, float y)
        {
            v.Set(v.x, y, v.z);
            return v;
        }

        public static Vector3 SetZ(this Vector3 v, float z)
        {
            v.Set(v.x, v.y, z);
            return v;
        }

//    public static Vector2 ClampVerticalToEllipse(Vector2 p, Vector2 ellipseSize)
//    {
//        var ptOnEllipse = GetPointOnEllipse(p, ellipseSize);
//
//        if (p.magnitude > ptOnEllipse.magnitude)
//            return ptOnEllipse;
//        else
//            return p;        
//    }
//
//    public static Vector2 GetPointOnEllipse(Vector2 p, Vector2 ellipseSize)
//    {
//        var baseEq = (ellipseSize.x * ellipseSize.y) / 
//                     Mathf.Sqrt(ellipseSize.x *ellipseSize.x * p.y * p.y + ellipseSize.y *ellipseSize.y * p.x* p.x);
//        var x = baseEq * p.x;
//        var y = baseEq * p.y;
//        return new Vector2(x, y);
//    }

        public static T GetRandom<T>(this List<T> list)
        {
            if (list == null || list.Count == 0)
                throw new IndexOutOfRangeException("List is null or empty");

            return list[Random.Range(0, list.Count)];
        }

        public static bool IsInRange(this int value, Vector2 range)
        {
            return ((float) value).IsInRange(range.x, range.y);
        }

        public static bool IsInRange(this float value, Vector2 range)
        {
            return value.IsInRange(range.x, range.y);
        }

        public static bool IsInRange(this float value, float min, float max, float ignoremaxValue = -1)
        {
            return value >= min && (value <= max || max == ignoremaxValue);
        }


        public static Vector3 ClampVerticalToEllipse(Vector3 vector, Vector3 maxVector)
        {
            vector.y = Vector3.ClampMagnitude(vector, maxVector.y).y;
            vector.x = Vector3.ClampMagnitude(vector, maxVector.x).x * maxVector.y / maxVector.x;
            return vector;
        }

        public static float GetTime(float velocity, float speed)
        {
            return velocity != 0 ? speed / velocity : 0;
        }

        public static float GetTime(Vector3 velocity, float speed)
        {
            return GetTime(velocity.magnitude, speed);
        }

        public static float GetAngleByVector(Vector3 velocity)
        {
            return Mathf.Atan2(velocity.y, velocity.x) * Mathf.Rad2Deg - 90;
        }
        
        public static int GetWeightedIndex(Dictionary<int, float> weightsList)
        {
            float sumWeight = 0;
            for (int i = 0; i < weightsList.Count; i++)
            {
                sumWeight += weightsList[i];
            }
        
            var rand = Random.Range(0, sumWeight);


            for (int i = 0; i < weightsList.Count; i++)
            {
                if (rand < weightsList[i])
                    return weightsList.Keys.ElementAt(i);
                rand -= weightsList[i];

            }

            return weightsList.Count-1;
        }
    }
}