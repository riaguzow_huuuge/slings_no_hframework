﻿using System;

namespace H.Games.Slings.Utilities
{
    [Serializable]
    public struct MinMax
    {
        public float min;
        public float max;

        public MinMax(float min, float max)
        {
            this.min = min;
            this.max = max;
        }

        public float GetRandom()
        {
            return UnityEngine.Random.Range(min, max);
        }

        public bool HaveInRange(float value)
        {
            return value >= min && value < max;
        }
    }
}