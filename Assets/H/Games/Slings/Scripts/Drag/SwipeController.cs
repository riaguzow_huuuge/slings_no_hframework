﻿using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using H.Games.Slings.CoreModules;
using H.Games.Slings.Trajectory;
using H.Games.Slings.Utilities;
using UnityEngine;

namespace H.Games.Slings.Drag
{
    public class SwipeController : MonoBehaviour
    {
        [SerializeField]
        TrajectoryDrawer trajectoryDrawer;

        DragModule dragModule;

        Vector2 maxSwipeVector;

        Vector2 MinSwipeVector
        {
            get { return maxSwipeVector / 2.5f; }
        }

        float power = 15000;

        VectorTweener vTweener;

        Vector2 dragVector
        {
            get { return vTweener.CurrentVector; }
        }

        bool isDraging = false;

        public bool IsDraging
        {
            get { return isDraging; }
        }

        void Awake()
        {
            dragModule = new DragModule();
            vTweener = new VectorTweener(0.07f);
            maxSwipeVector = BounduaryController.GetRatioVector(5);
            var pauseListener = gameObject.GetOrAddComponent<PauseListener>();
            pauseListener.AddPauseAction(ResetDrag);
        }

        void ResetDrag(bool isPaused)
        {
            isDraging = false;
            trajectoryDrawer.Hide();
            vTweener.Reset();
        }

        public void StartDrag(Vector3 touchPos)
        {
            if (isDraging)
                return;
            dragModule.StartDrag(touchPos);
            Core.Player.StartStretch();
            Core.UI.ShowTutorialSecondStep();
            isDraging = true;
        }

        public void UpdateDrag(Vector3 touchPos)
        {
            if (Core.Player.isBallThrown || !isDraging)
                return;
            UpdateForce(touchPos);

            Core.Player.Stretch(dragVector.magnitude / maxSwipeVector.magnitude);
            DrawTrajectory();
            Rotatebasket();
        }

        public void EndDrag(Vector3 touchPos)
        {
            isDraging = false;
            if (Core.Player.isBallThrown)
                return;
            if (IsMinForce())
            {
                Core.Player.Throw(GetCurrentVelocity());
                trajectoryDrawer.Hide();
                Core.Audio.PlayJump();
                Core.UI.HideTutorial();
            }
            else
            {
                Core.Player.ResetToZero();
                Core.UI.ShowTutorialFirstStep();
            }

            vTweener.Reset();
            isDraging = false;
        }

        void DrawTrajectory()
        {
            float alpha = !IsMinForce() ? 0 : dragVector.magnitude / maxSwipeVector.magnitude * 2;
            trajectoryDrawer.Draw(Core.Player.transform.position, GetCurrentVelocity(), 100, alpha);
        }

        Vector2 GetCurrentVelocity()
        {
            return power * new Vector2(dragVector.x / maxSwipeVector.x * maxSwipeVector.x / maxSwipeVector.y,
                       dragVector.y / maxSwipeVector.y);
        }

        bool IsMinForce()
        {
            return dragVector.magnitude > MinSwipeVector.magnitude;
        }

        void Rotatebasket()
        {
            if (dragVector == Vector2.zero)
                return;
            float angle = SlingsUtils.GetAngleByVector(dragVector);
            Core.Game.CurrentBasket.SetRotation(new Vector3(0, 0, angle));
        }

        void UpdateForce(Vector2 touchPos)
        {
            Vector2 v = GetClampedSwipeVector(touchPos);
            vTweener.Update(v);
        }

        Vector3 GetClampedSwipeVector(Vector3 touchPos)
        {
            return SlingsUtils.ClampVerticalToEllipse(GetSwipeVector(touchPos), maxSwipeVector);
        }

        Vector3 GetSwipeVector(Vector3 touchPos)
        {
            return dragModule.GetDragVector(touchPos);
        }
    }

    public class VectorTweener
    {
        TweenerCore<Vector2, Vector2, VectorOptions> vectorTweener;

        readonly float tweeningTime;
        Vector2 currentVector = Vector2.zero;

        public Vector2 CurrentVector
        {
            get { return currentVector; }
        }

        public VectorTweener(float tweening)
        {
            this.tweeningTime = tweening;
        }

        public void Update(Vector2 destVector)
        {
            if (vectorTweener != null)
                vectorTweener.Kill();
            vectorTweener = DOTween.To(() => currentVector, x => currentVector = x, destVector, tweeningTime);
        }

        public void Reset()
        {
            vectorTweener.Kill();
            currentVector = Vector2.zero;
        }
    }
}