﻿using UnityEngine;

namespace H.Games.Slings.Drag
{
    public class DragModule
    {
        Vector3 startTouch;

        public void StartDrag(Vector3 touchPosition)
        {
            this.startTouch = touchPosition;
        }

        public Vector3 GetDragVector(Vector3 touchPosition)
        {
            return GetDrag(touchPosition);
        }

        Vector2 GetDrag(Vector3 currentTouch)
        {
            return -GetDragBetween(startTouch, currentTouch);
        }
    
        Vector2 GetDragBetween(Vector3 fromPos, Vector3 toPos)
        {
            return toPos - fromPos;
        }
    }
}