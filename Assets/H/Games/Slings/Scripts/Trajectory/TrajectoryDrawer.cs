﻿using System.Collections.Generic;
using H.Games.Slings.CoreModules;
using UnityEngine;

namespace H.Games.Slings.Trajectory
{
    public class TrajectoryDrawer : MonoBehaviour
    {
        [SerializeField]
        GameObject trajectoryPointPrefeb;
        List<SpriteRenderer> trajectoryPoints;

        int numOfTrajectoryPoints = 10;
        float originPointStep = 0.05f;
        float pointStep = 0.05f;

        void Start()
        {
            Prepare();
            SpawnPoints();
        }

        void Prepare()
        {
            numOfTrajectoryPoints = Core.Difficulty.TrajectoryDotsCount;
            originPointStep = Core.Difficulty.TrajectoryOriginStep;
            pointStep = Core.Difficulty.TrajectoryDotsStep;
        }

        void SpawnPoints()
        {
            trajectoryPoints = new List<SpriteRenderer>();
            for (int i = 0; i < numOfTrajectoryPoints; i++)
            {
                var dot = Instantiate(trajectoryPointPrefeb).GetComponent<SpriteRenderer>();
                dot.enabled = false;
                dot.transform.localScale = trajectoryPointPrefeb.transform.localScale * Mathf.Lerp(1, 0.5f, i / (float)numOfTrajectoryPoints);
            
                trajectoryPoints.Add(dot);
            }
        }

        public void Draw(Vector3 startPosition, Vector3 velocity, float ballMass, float alpha)
        {
            SetTrajectoryPoints(startPosition, velocity / ballMass, alpha);
        }
    
        void SetTrajectoryPoints(Vector3 pStartPosition, Vector3 pVelocity, float alpha)
        {
            var points = TrajectoryCalculator.CalculateTrajectory(pStartPosition, pVelocity, numOfTrajectoryPoints, originPointStep, pointStep);
            for (int i = 0; i < numOfTrajectoryPoints; i++)
            {
                trajectoryPoints[i].transform.position = points[i].position;
                trajectoryPoints[i].enabled = true;
                trajectoryPoints[i].color = new Color(1, 1,1, alpha);
                trajectoryPoints[i].transform.eulerAngles = points[i].rotation;
            }
        }

        public void ResetandHide()
        {
            Draw(transform.position, Vector3.zero, 1, 0);
            Hide();
        }

        public void Hide()
        {
            for (int i = 0; i < trajectoryPoints.Count; i++)
                trajectoryPoints[i].enabled = false;
        }
    }
}