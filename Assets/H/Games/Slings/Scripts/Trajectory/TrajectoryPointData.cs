﻿using UnityEngine;

namespace H.Games.Slings.Trajectory
{
    public struct TrajectoryPointData
    {
        public Vector3 position;
        public Vector3 rotation;

        public TrajectoryPointData(Vector3 position, Vector3 rotation)
        {
            this.position = position;
            this.rotation = rotation;
        }
    }
}