﻿using System.Collections.Generic;
using H.Games.Slings.CoreModules;
using UnityEngine;

namespace H.Games.Slings.Trajectory
{
    public static class TrajectoryCalculator
    {
        public static TrajectoryPointData[] CalculateTrajectory(Vector3 pStartPosition, Vector3 pVelocity, int size,
            float originStep, float step)
        {
            var trajectoryPoints = new TrajectoryPointData[size];

            float velocity = Mathf.Sqrt((pVelocity.x * pVelocity.x) + (pVelocity.y * pVelocity.y));
            float angle = Mathf.Rad2Deg * (Mathf.Atan2(pVelocity.y, pVelocity.x));
            float fTime = 0;

            List<Mirror> mirrors = new List<Mirror>();

            fTime += originStep;
            for (int i = 0; i < size; i++)
            {
                float dx = velocity * fTime * Mathf.Cos(angle * Mathf.Deg2Rad);
                float dy = velocity * fTime * Mathf.Sin(angle * Mathf.Deg2Rad) -
                           (Physics2D.gravity.magnitude * fTime * fTime / 2.0f);
                Vector3 pos = new Vector3(pStartPosition.x + dx, pStartPosition.y + dy, 0);
                var pAngle = new Vector3(0, 0,
                    Mathf.Atan2(pVelocity.y - (Physics2D.gravity.magnitude) * fTime, pVelocity.x) * Mathf.Rad2Deg);

                pos = Mirror(pos, mirrors, i, trajectoryPoints);

                trajectoryPoints[i] = new TrajectoryPointData(pos, pAngle);

                fTime += step;
            }

            return trajectoryPoints;
        }

        // This method is was created with tears and pain. Please, don't rate the author.
        static Vector3 Mirror(Vector3 pos, List<Mirror> mirrors, int i, TrajectoryPointData[] trajectoryPoints)
        {
            for (int j = 0; j < mirrors.Count; j++)
                pos = Translate(pos, mirrors[j]);

            if (pos.x > Core.Bounduary.RightGameplayBound)
            {
                var m = new Mirror(new Vector3(Core.Bounduary.RightGameplayBound, 0), 1);
                pos = Translate(pos, m);
                mirrors.Add(m);
            }
            else if (pos.x < Core.Bounduary.LeftGameplayBound)
            {
                var m = new Mirror(new Vector3(Core.Bounduary.LeftGameplayBound, 0), -1);
                pos = Translate(pos, m);
                mirrors.Add(m);
            }

            else if (Core.Game.NextBasket.HasBlocker())
            {
                var m = new Mirror();
                m.Type = Core.Game.NextBasket.Blocker().IsVertical() ? MirrorType.Vertical : MirrorType.Horizontal;

                if (i == 0)
                    m.MirrorPoint = BlockerBounds.ClosestPoint(Core.Player.transform.position);
                else
                    m.MirrorPoint = BlockerBounds.ClosestPoint(trajectoryPoints[i - 1].position);

                if (Core.Game.NextBasket.Blocker().IsHorizontal())
                {
                    if (m.MirrorPoint.x == BlockerBounds.max.x)
                        m.MirrorSign = -1;
                    else
                        m.MirrorSign = 1;
                }
                else
                {
                    if (m.MirrorPoint.y == BlockerBounds.max.y)
                        m.MirrorSign = -1;
                    else
                        m.MirrorSign = 1;
                }

                if (CrossesBlocker(pos, m))
                {
                    pos = Translate(pos, m);
                    mirrors.Add(m);
                }
            }

            return pos;
        }

        static bool CrossesBlocker(Vector3 pos, Mirror m)
        {
            return CrossesBlockerHorizontally(pos, m) && CrossesBlockerVertically(pos, m);
        }

        static bool CrossesBlockerVertically(Vector3 pos, Mirror m)
        {
            if (m.Type == MirrorType.Horizontal)
                return pos.y > BlockerBounds.min.y && pos.y < BlockerBounds.max.y;
            else
                return pos.x > BlockerBounds.min.x && pos.x < BlockerBounds.max.x;
        }

        static bool CrossesBlockerHorizontally(Vector3 pos, Mirror m)
        {
            if (m.Type == MirrorType.Horizontal)
                return (m.MirrorSign > 0 && pos.x > m.MirrorPoint.x) || (m.MirrorSign < 0 && pos.x < m.MirrorPoint.x);
            else
                return (m.MirrorSign > 0 && pos.y > m.MirrorPoint.y) || (m.MirrorSign < 0 && pos.y < m.MirrorPoint.y);
        }

        static Vector3 Translate(Vector3 pos, Mirror mirror)
        {
            if (mirror.Type == MirrorType.Horizontal)
            {
                var value = mirror.MirrorPoint.x + Mathf.Abs(mirror.MirrorPoint.x - pos.x) * -mirror.MirrorSign;
                return new Vector3(value, pos.y);
            }
            else
            {
                var value =  mirror.MirrorPoint.y + Mathf.Abs(mirror.MirrorPoint.y - pos.y) * -mirror.MirrorSign;
                return new Vector3(pos.x, value);

            }
        }
        
        static Bounds BlockerBounds
        {
            get { return Core.Game.NextBasket.Blocker().GetBounds(); }
        }
    }

    struct Mirror
    {
        public Vector3 MirrorPoint;
        public float MirrorSign;
        public MirrorType Type;

        public Mirror(Vector3 mirrorPoint, float mirrorSign, MirrorType type = MirrorType.Horizontal)
        {
            this.MirrorPoint = mirrorPoint;
            this.MirrorSign = mirrorSign;
            this.Type = type;
        }
    }

    enum MirrorType
    {
        Horizontal,
        Vertical
    }
}