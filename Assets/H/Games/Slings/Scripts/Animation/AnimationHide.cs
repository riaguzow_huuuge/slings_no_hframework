﻿using UnityEngine;

namespace H.Games.Slings.Animation
{
    public class AnimationHide : MonoBehaviour
    {
        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}