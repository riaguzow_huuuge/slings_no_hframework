﻿using System;
using UnityEngine;

namespace H.Games.Slings.Baskets
{
    [Serializable]
    public class BasketSpawnConfig
    {
        [SerializeField]
        BasketsPreset basketsPreset;

        public BasketsPreset BasketsPreset
        {
            get { return basketsPreset; }
        }
    
        [SerializeField]
        Vector2 spawnPointsRange = new Vector2(0, -1);

        [SerializeField, Range(1, 10)]
        int spawnPropabilityWeight = 5;
    
        [SerializeField]
        SpawnType type;
    
        public Vector2 SpawnPointsRange
        {
            get { return spawnPointsRange; }
        }

        public int SpawnPropabilityWeight
        {
            get { return spawnPropabilityWeight; }
        }

        public SpawnType Type
        {
            get { return type; }
        }

        public bool IsTypeOf(SpawnType t)
        {
            return type == t || type == SpawnType.Both;
        }
    }
}