﻿using H.Games.Slings.CoreModules;
using UnityEngine;

namespace H.Games.Slings.Baskets
{
    public class BasketMask : MonoBehaviour
    {
        [SerializeField]
        GameObject mask;

        void Start()
        {
            Deactivate();
        }
    
        void OnTriggerEnter2D(Collider2D other)
        {
            if (Core.Player.isBallThrown)
                Activate();
        }

        void OnTriggerExit2D(Collider2D other)
        {
            Deactivate();
        }

        public void Activate()
        {
            mask.SetActive(true);
        }
    
        public void Deactivate()
        {
            mask.SetActive(false);
        }
    }
}