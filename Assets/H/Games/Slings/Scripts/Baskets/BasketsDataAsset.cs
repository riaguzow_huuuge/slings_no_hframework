﻿using System.Collections.Generic;
using System.Linq;
using H.Games.Slings.CoreModules;
using H.Games.Slings.Utilities;
using SubjectNerd.Utilities;
using UnityEngine;

namespace H.Games.Slings.Baskets
{
    [CreateAssetMenu(fileName = "BasketsDataAsset", menuName = "Slings/BasketsDataAsset")]
    public class BasketsDataAsset : ScriptableObject
    {
        [SerializeField, Reorderable]
        List<BasketSpawnConfig> basketsPresets = new List<BasketSpawnConfig>();

        public BasketSpawnConfig GetRandomBasket(SpawnType type)
        {
            var b = basketsPresets.Where(x =>
                x.IsTypeOf(type) && Core.ScoreController.CurrentScore.IsInRange(x.SpawnPointsRange)).OrderBy(x => x.SpawnPropabilityWeight);
            return GetRandomByType(b.ToList());
        }

        static BasketSpawnConfig GetRandomByType(List<BasketSpawnConfig> data)
        {
            int sumWeight = 0;
            for (int i = 0; i < data.Count; i++)
            {
                sumWeight += data[i].SpawnPropabilityWeight;
            }
        
            var rand = Random.Range(0, sumWeight);


            for (int i = 0; i < data.Count; i++)
            {
                if (rand < data[i].SpawnPropabilityWeight)
                    return data[i];
                rand -= data[i].SpawnPropabilityWeight;

            }

            return data[data.Count-1];
        }
    }
}