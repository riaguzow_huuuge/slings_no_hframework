﻿using System.Collections.Generic;
using System.Linq;
using SubjectNerd.Utilities;
using UnityEngine;

namespace H.Games.Slings.Baskets
{
    [CreateAssetMenu(fileName = "PresetBasketsConfig", menuName = "Slings/PresetBasketsConfig")]
    public class BasketsPreset : ScriptableObject
    {
        [HideInInspector]
        public bool isLocked;
    
        [SerializeField, Reorderable]
        List<BasketSpawnData> basketsList;

        public List<BasketSpawnData> BasketsList
        {
            get { return basketsList; }
        }

        public void AddBasket(BasketSpawnData b)
        {
            if (basketsList == null)
                basketsList = new List<BasketSpawnData>();
            basketsList.Add(b);
        }

#if UNITY_EDITOR
        public void Transform()
        {
            basketsList = basketsList.OrderBy(x => x.Position.y).ToList();
            for (var i = 0; i < basketsList.Count; i++)
            {
                if (i == 0)
                {
                    basketsList[i].XPosition = basketsList[i].Position.x;
                    basketsList[i].YOffset = 0;
                    if (basketsList.Count == 1)
                        basketsList[i].Randomize = true;
                }
                else
                {
                    basketsList[i].XPosition = basketsList[i].Position.x;
                    basketsList[i].YOffset = basketsList[i].Position.y - basketsList[i-1].Position.y;
                
                }
            }
        }

        public void Clear()
        {
            basketsList.Clear();
        }

        public void ChangeLock()
        {
            isLocked = !isLocked;
        }
#endif
    }
}