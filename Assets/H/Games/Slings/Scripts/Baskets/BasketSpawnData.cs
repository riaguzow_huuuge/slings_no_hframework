﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace H.Games.Slings.Baskets
{
    [Serializable]
    public class BasketSpawnData
    {
        public BasketSpawnData(Basket basket, Vector2 position)
        {
            this.basket = basket;
            this.Position = position;
        }

        public Basket Basket
        {
            get { return basket; }
        }

        public Vector2 Position { get; private set; }

        public float YOffset
        {
            get { return yOffset; }
            set { yOffset = value; }
        }

        public float XPosition
        {
            get { return xPosition; }
            set { xPosition = value; }
        }

        public bool Randomize
        {
            get { return randomize; }
            set { randomize = value; }
        }

        public List<CoinPathElement> CoinPath
        {
            get { return coinPath; }
            set { coinPath = value; }
        }
        
        public bool MoveCoinsWithBasket
        {
            get { return moveCoinsWithBasket; }
        }

        [SerializeField]
        Basket basket;
    
        [SerializeField]
        float yOffset;
    
        [SerializeField]
        float xPosition;

        [SerializeField]
        bool randomize;

        [SerializeField]
        List<CoinPathElement> coinPath = new List<CoinPathElement>();

        [SerializeField]
        bool moveCoinsWithBasket = false;
    }
}