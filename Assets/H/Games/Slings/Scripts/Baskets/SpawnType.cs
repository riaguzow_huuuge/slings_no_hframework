﻿namespace H.Games.Slings.Baskets
{
    public enum SpawnType
    {
        Left = 0, 
        Right = 1,
        Both = 2
    }
}