﻿using UnityEngine;

namespace H.Games.Slings.Baskets
{
    [System.Serializable]
    public struct CoinBurstData
    {
        [SerializeField]
        float burstAngle;

        [SerializeField]
        int burstCount;
        
        [SerializeField]
        float burstRadius;

        public float BurstAngle
        {
            get { return burstAngle; }
        }

        public int BurstCount
        {
            get { return burstCount; }
        }

        public float BurstRadius
        {
            get { return burstRadius; }
        }


    }
}