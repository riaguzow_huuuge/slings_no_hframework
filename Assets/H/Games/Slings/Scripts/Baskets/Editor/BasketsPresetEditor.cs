﻿using System.Linq;
using H.Games.Slings.Baskets;
using H.Games.Slings.Environment;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BasketsPreset))]
public class BasketsPresetEditor : Editor
{
    private SerializedObject obj;
    BasketsPreset basketsPreset;

    public void OnEnable ()
    {
        obj = new SerializedObject (target);
        basketsPreset = target as BasketsPreset;
    }
    
    public override void OnInspectorGUI()
    {
        GUI.enabled = !basketsPreset.isLocked;
        
        DropAreaGUI();
        GUI.enabled = true;
        base.OnInspectorGUI();
        GUI.enabled = !basketsPreset.isLocked;
        if (GUILayout.Button("Transform"))
        {
            basketsPreset.Transform();
            basketsPreset.ChangeLock();
            EditorUtility.SetDirty(basketsPreset);
        }

        GUI.enabled = true;
        if (GUILayout.Button("Clear") && EditorUtility.DisplayDialog("Clear list", "Do you want to clear list?", "Yup", "Don't do it, god damn it!"))
        {
            basketsPreset.Clear();
            basketsPreset.isLocked = false;
        }
        
        if (GUILayout.Button("Show"))
        {
            Show();
        }
    }
    
    public void DropAreaGUI ()
    {
        Event evt = Event.current;
        Rect drop_area = GUILayoutUtility.GetRect (0.0f, 50.0f, GUILayout.ExpandWidth (true));
        GUI.Box (drop_area, "Drop baskets");
     
        switch (evt.type) {
            case EventType.DragUpdated:
            case EventType.DragPerform:
                if (!drop_area.Contains (evt.mousePosition))
                    return;
             
                DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
         
                if (evt.type == EventType.DragPerform) {
                    DragAndDrop.AcceptDrag ();
             
                    foreach (Object dragged_object in DragAndDrop.objectReferences)
                    {
                        if (dragged_object.GetType() != typeof(GameObject))
                            return;
                        var basket = ((GameObject)dragged_object).GetComponent<Basket>();
                        if (basket)
                        {
                            AddBasket(basket);
                        }
                        else
                        {
                            Debug.Log(string.Format("{0} is not type of {1}", dragged_object.name, typeof(Basket)), dragged_object);
                        }
                    }
                }
                break;
        }
    }

    void AddBasket(Basket basket)
    {
        var basketprefab = PrefabUtility.GetPrefabParent(basket) as Basket;
        if (basketprefab)
        {
            var data = new BasketSpawnData(basketprefab, basket.transform.position);
            foreach (var coin in basket.GetComponentsInChildren<Coin>())
            {
                data.CoinPath.Add(new CoinPathElement(coin.transform.localPosition));
            }
            basketsPreset.AddBasket(data);
        }
        else if(PrefabUtility.GetPrefabObject(basket))
        {
            var data = new BasketSpawnData(basket, Vector3.zero);
            basketsPreset.AddBasket(data);
        }
        else
        {
             Debug.Log(string.Format("{0} is not prefab instance", basket.name), basket);
        }
    }
    
    public void Show()
    {
        for (var i = 0; i < basketsPreset.BasketsList.Count; i++)
        {
            Basket a = PrefabUtility.InstantiatePrefab(basketsPreset.BasketsList[i].Basket) as Basket;
            if (i == 0)
                a.transform.position = new Vector3(basketsPreset.BasketsList[i].XPosition, basketsPreset.BasketsList[i].Position.y);
            else
            {
                a.transform.position = new Vector2(basketsPreset.BasketsList[i].XPosition, basketsPreset.BasketsList.First().Position.y + Sum(i));
            }
        }
    }

    float Sum(int idx)
    {
        float sum = 0;
        for (var i = 0; i <= idx; i++)
            sum += basketsPreset.BasketsList[i].YOffset;
        return sum;
    }
}