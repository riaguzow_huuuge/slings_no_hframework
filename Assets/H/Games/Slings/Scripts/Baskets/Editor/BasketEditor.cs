﻿using H.Games.Slings.Baskets;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Basket))]
public class BasketEditor : Editor
{
    private SerializedObject obj;
    Basket basket;

    public void OnEnable()
    {
        obj = new SerializedObject(target);
        basket = target as Basket;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Show Organs"))
        {
            var pref = FindObjectOfType<BasketSpawner>().BasketOrgansPrefab;
            var o = Instantiate(pref, basket.transform);
            o.transform.localPosition = Vector3.zero;
            o.transform.rotation = Quaternion.identity;
        }
    }
}