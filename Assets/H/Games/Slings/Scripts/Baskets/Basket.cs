﻿using System.Collections.Generic;
using DG.Tweening;
using H.Games.Slings.CoreModules;
using H.Games.Slings.Environment;
using UnityEngine;

namespace H.Games.Slings.Baskets
{
    public class Basket : MonoBehaviour
    {
        [SerializeField]
        List<int> rotationAngles;

        public List<int> RotationAngles
        {
            get { return rotationAngles; }
        }

        [SerializeField]
        public bool isFirst = false;

        [SerializeField]
        Blocker blocker;

        BasketOrgans organs;
        List<Coin> coin = new List<Coin>();

        public bool IsStarBox { get; set; }
        bool isClearShot = true;
        bool isScored = false;

        void Awake1()
        {
            if (isFirst)
            {
                Core.Game.SetFirstBasket(this);
                SetCollidersActive(false);
            }
            else
            {
                organs = Instantiate(Core.BasketSpawner.BasketOrgansPrefab, this.transform);
                organs.transform.localPosition = Vector3.zero;
                organs.transform.localRotation = Quaternion.identity;
            }
        }

        void Start()
        {
            Awake1();
            DecideIfStarBox();
            PlayShowAnimation();
        }

        void DecideIfStarBox()
        {
            if (IsStarBox)
                organs.ActivateStarBox();
        }

        void PlayShowAnimation()
        {
            transform.localScale = Vector3.zero;
            transform.DOScale(Vector3.one, 0.3f).OnComplete(ShowBlocker);
        }

        void ShowBlocker()
        {
            if (blocker)
                blocker.Show();
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
                OnScore();
        }

        void OnScore()
        {
            if (!isScored)
                Core.ScoreController.CountClearShot(isClearShot);
            isScored = true;

            SetCollidersActive(false);

            if (IsStarBox)
            {
                organs.DeactivateStarBox();
                Core.CoinSpawner.SpawnCoinBursts(transform.position);
                IsStarBox = false;
            }
            else
            {
                SpawnSmoke();
            }

            DestroyCoin();
            
            Animate();
            Core.Game.OnBasketScore(this);
            ResetRotation();

            var movement = GetComponent<BasketMovement>();
            if (movement)
                movement.enabled = false;

            if (blocker)
                blocker.Hide();
        }

        void SpawnSmoke()
        {
            var smoke = Instantiate(Core.Player.boxParts);
            smoke.transform.SetParent(this.transform);
            smoke.transform.localPosition = new Vector3(0, 5, 0);
            smoke.transform.SetParent(null);
        }

        void Animate()
        {
            organs.Animate();
        }

        public void SetCollidersActive(bool isActive)
        {
            if (organs)
                organs.SetColliders(isActive);
            BoxCollider2D b = GetComponent<BoxCollider2D>();
            if (b)
                b.enabled = isActive;
        }

        public void ResetRotation()
        {
            var time = isFirst ? 0 : 0.2f;
            transform.DOLocalRotate(Vector3.zero, time);
        }

        public void SetRotation(Vector3 rotation)
        {
            transform.eulerAngles = rotation;
        }

        public void ActivateColliders()
        {
            SetCollidersActive(true);
        }

        public void Drop()
        {
            var endValue = transform.position.y - 80;
            var seq = DOTween.Sequence();
            seq.Append(Shaking()).Insert(0.5f, transform.DOMoveY(endValue, 3f).SetEase(Ease.OutCubic))
                .OnComplete(Destroy);

            SetCollidersActive(false);
        }

        Tweener Shaking()
        {
            return transform.DOShakeRotation(0.6f, new Vector3(0, 0, 70), 8, 40);
        }

        void Destroy()
        {
            Destroy(gameObject);
        }

        public void ResetClearShot()
        {
            isClearShot = false;
        }

        public void SetWaitingState()
        {
            if (organs)
                organs.ActivateMask();
        }

        public void SetPullingState()
        {
            if (organs)
                organs.DeactivateMask();
        }

        public void SetCoin(Coin c)
        {
            coin.Add(c);
        }

        public void SetCoins(List<Coin> c)
        {
            coin.AddRange(c);
        }

        void DestroyCoin()
        {
            for (int i = 0; i < coin.Count; i++)
            {
                if (coin[i])
                    coin[i].Hide();
            }
        }

        public bool HasBlocker()
        {
            return blocker !=null;
        }

        public Blocker Blocker()
        {
            return blocker;
        }
    }
}