﻿using System.Linq;
using DG.Tweening;
using H.Games.Slings.CoreModules;
using H.Games.Slings.Utilities;
using UnityEngine;

namespace H.Games.Slings.Baskets
{
    [RequireComponent(typeof(LineRenderer)), RequireComponent(typeof(Basket))]
    public class BasketMovement : MonoBehaviour
    {
        [SerializeField]
        MovementType movementType;

        [SerializeField]
        float speed;

        [SerializeField]
        LoopType loopType = LoopType.Yoyo;

        [SerializeField]
        Ease ease = Ease.InOutSine;

        [SerializeField]
        Vector3[] points;

        LineRenderer line;

        LineRenderer Line
        {
            get
            {
                if (line == null)
                    line = GetComponent<LineRenderer>();
                return line;
            }
        }
    
        float GetSpeedFactor()
        {
            if (movementType == MovementType.Horizontal)
                return Core.Difficulty.HorizontalBasketsSpeedFactor;
            else if (movementType == MovementType.Vertical)
                return Core.Difficulty.VerticalBasketsSpeedFactor;
            return 1;
        }

        float GetTierFactor()
        {
            if (Core.Difficulty.SpeedTiers.Count == 0)
                return 1;
            
            for (int i = 0; i < Core.Difficulty.SpeedTiers.Count; i++)
            {
                if (Core.Difficulty.SpeedTiers[i].PointsRange.HaveInRange(Core.ScoreController.CurrentScore))
                {
                    return Core.Difficulty.SpeedTiers[i].SpeedFactor;
                }
            }
            return Core.Difficulty.SpeedTiers.Last().SpeedFactor;
        }
    
        void Awake()
        {
            var pauseListener = gameObject.GetOrAddComponent<PauseListener>();
            pauseListener.AddPauseAction(OnGamePaused);
        }

        void OnGamePaused(bool isPaused)
        {
            if (isPaused)
                movement.Pause();
            else
                movement.Play();
        }

        void Start()
        {
            TranslatePoints();
            ActivateMovement();
        }

        void OnDisable()
        {
            HideLine();
            StopMovement();
        }

        void TranslatePoints()
        {
            for (int i = 0; i < points.Length; i++)
            {
                points[i] = transform.position + points[i];
            }
        }
        
        void ActivateMovement()
        {
            ShowLine(points);
            StartMovement(points);

        }

        void ShowLine(Vector3[] pts)
        {
            Line.positionCount = pts.Length;
            Line.SetPositions(pts);
        }

        void HideLine()
        {
            Line.enabled = false;
        }

        Tweener movement;
        void StartMovement(Vector3[] path)
        {
            var time = GetDistance(path) / (speed * GetSpeedFactor() * GetTierFactor());
            movement = transform.DOPath(path, time).SetEase(ease).SetLoops(-1, loopType);
        }

        void StopMovement()
        {
            movement.Kill();
        }

        static float GetDistance(Vector3[] pList)
        {
            float dist = 0;
            for (int i = 0; i < pList.Length-1; i++)
            {
                dist += Vector3.Distance(pList[i], pList[i + 1]);
            }

            return dist;
        }

        enum MovementType
        {
            Horizontal = 0,
            Vertical = 1
        }
    }
}