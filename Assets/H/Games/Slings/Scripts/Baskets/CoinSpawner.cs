﻿using System.Collections.Generic;
using DG.Tweening;
using H.Games.Slings.Environment;
using H.Games.Slings.Utilities;
using UnityEngine;

namespace H.Games.Slings.Baskets
{
    public class CoinSpawner : MonoBehaviour
    {
        [SerializeField]
        Coin coinPrefab;

        [SerializeField]
        float coinHeight;

        [SerializeField]
        MinMax burstCollectDelay = new MinMax(1f, 1.2f);

        [SerializeField]
        List<CoinBurstData> burstsData;

        public void SpawnCoinAboveBasket(Basket basket)
        {
            var pos = basket.transform.position + Vector3.up * coinHeight;
            var c = SpawnCoin(pos, basket.transform);
            c.transform.RotateAround(basket.transform.position, Vector3.forward,
                basket.transform.rotation.eulerAngles.z);
            basket.SetCoin(c);
        }

        public void SpawnCoinPath(Basket basket, List<CoinPathElement> coinPath, bool moveWithBasket)
        {
            var coins = new List<Coin>();
            for (int i = 0; i < coinPath.Count; i++)
            {
                var c = SpawnCoin(basket.transform.position, basket.transform);
                c.transform.localPosition = coinPath[i].LocalPosition;
                if (!moveWithBasket)
                    c.transform.SetParent(null);
                coins.Add(c);
            }

            basket.SetCoins(coins);
        }

        public Coin SpawnCoin(Vector3 position, Transform parent = null)
        {
            var c = Instantiate(coinPrefab, position, Quaternion.identity);
            c.transform.SetParent(parent);
            return c;
        }

        public void SpawnCoinBursts(Vector3 position)
        {
            for (int i = 0; i < burstsData.Count; i++)
            {
                var points = GetPointsOnCircle(burstsData[i].BurstCount, burstsData[i].BurstAngle,
                    burstsData[i].BurstRadius);
                SpawnCoinBurst(position, points);
            }
        }

        void SpawnCoinBurst(Vector3 position, Vector3[] points)
        {
            for (int i = 0; i < points.Length; i++)
            {
                var coin = SpawnCoin(position);
                coin.Collect(burstCollectDelay.GetRandom());
                coin.transform.DOMove(position + points[i], 0.2f).SetDelay(Random.Range(0, 0.3f));
            }
        }

        static Vector3[] GetPointsOnCircle(int count, float angleRange, float radius)
        {
            var array = new Vector3[count];
            var angleStep = angleRange / count;
            var startAngle = 90 - angleRange / 2 + angleStep / 2;

            for (int i = 0; i < count; i++)
                array[i] = GetPointOnCircle(startAngle + angleStep * i, radius);
            return array;
        }

        static Vector3 GetPointOnCircle(float angle, float radius)
        {
            return new Vector3(radius * Mathf.Cos(angle * Mathf.Deg2Rad), radius * Mathf.Sin(angle * Mathf.Deg2Rad));
        }
    }
}