﻿using System.Collections.Generic;
using System.Linq;
using H.Games.Slings.Configs;
using H.Games.Slings.CoreModules;
using H.Games.Slings.Development;
using H.Games.Slings.Utilities;
using UnityEngine;


namespace H.Games.Slings.Baskets
{
    public class BasketSpawner : MonoBehaviour
    {
        [SerializeField]
        BasketsDataAsset baskets;

        [SerializeField]
        BasketsDataAsset presets;

        [SerializeField, MinMaxSlider(0, 1)]
        Vector2 xRange;

        MinMax yRange;

        Queue<BasketSpawnData> basketsQueue = new Queue<BasketSpawnData>();

        [SerializeField]
        BasketOrgans basketOrgansPrefab;

        public BasketOrgans BasketOrgansPrefab
        {
            get { return basketOrgansPrefab; }
        }

        [SerializeField]
        PresetName presetName;
        
        StarChances StarChances
        {
            get { return Core.Difficulty.StarBoxChances; }
        }

        void Awake()
        {
            yRange = Core.Difficulty.VerticalSpawnRange;
        }

        public Basket SpawnNext()
        {
            if (!basketsQueue.Any())
                SetUpQueue();

            var spawnData = basketsQueue.Dequeue();

            var p = GetPosition(spawnData);
            var rot = GetRotation(spawnData.Basket.RotationAngles);

            var b = Instantiate(spawnData.Basket, p, rot);
            DecideAboutCoins(b, spawnData);

            return b;
        }


        void SetUpQueue()
        {
            BasketSpawnConfig spawnConfig;
            if (Random.value > 0.7f)
                spawnConfig = presets.GetRandomBasket(GetSpawnType());
            else
                spawnConfig = baskets.GetRandomBasket(GetSpawnType());

            var b = spawnConfig.BasketsPreset.BasketsList;

            for (int i = 0; i < b.Count; i++)
                basketsQueue.Enqueue(b[i]);

            if (presetName != null)
                presetName.SetPresetName(spawnConfig.BasketsPreset.name);
        }

        SpawnType GetSpawnType()
        {
            var pos = Core.Game.CurrentBasket.transform.position;
            return pos.x < 0 ? SpawnType.Right : SpawnType.Left;
        }

        Vector2 GetPosition(BasketSpawnData spawnData)
        {
            var x = spawnData.Randomize ? GetSpawnPosition().x : spawnData.XPosition;

            float y;
            if (Mathf.Approximately(spawnData.YOffset, 0))
                y = GetSpawnPosition().y;
            else
                y = spawnData.YOffset + Core.Game.CurrentBasket.transform.position.y;


            return new Vector2(x, y);
        }

        Vector2 GetSpawnPosition()
        {
            var currentPos = Core.Game.CurrentBasket.transform.position;
            var c = Core.Bounduary.GameplayWidth / 2 - 8;

            float x;
            if (currentPos.x > 0)
                x = Random.Range(-c, currentPos.x - 15);
            else
                x = Random.Range(currentPos.x + 15, c);

            var y = currentPos.y + Random.Range(yRange.min, yRange.max);

            return new Vector3(x, y, 0);
        }

        Quaternion GetRotation(List<int> rotationAngles)
        {
            var angle = rotationAngles.Count == 0
                ? 0
                : rotationAngles.GetRandom() *
                  Mathf.Sign(transform.position.x - Core.Game.CurrentBasket.transform.position.x);
            return Quaternion.Euler(0, 0, angle);
        }

        void DecideAboutCoins(Basket b, BasketSpawnData spawnData)
        {
            var dict = new Dictionary<int, float>
            {
                {0, StarChances.StarBoxChance},
                {1, StarChances.StarChance},
                {2, StarChances.NoStarsChance}
            };
            
            if (spawnData.CoinPath.Count > 0)
                dict.Add(3, StarChances.StarPathChance);

            var i = SlingsUtils.GetWeightedIndex(dict);
            
            
            switch (i)
            {
                case 0:
                    b.IsStarBox = true;
                    break;
                case 1:
                    Core.CoinSpawner.SpawnCoinAboveBasket(b);
                    break;
                case 3:
                    Core.CoinSpawner.SpawnCoinPath(b, spawnData.CoinPath, spawnData.MoveCoinsWithBasket);
                    break;
                case 2:
                    return;
            }
        }
    }
}