﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace H.Games.Slings.Baskets
{
    public class BasketOrgans : MonoBehaviour
    {
    
        [SerializeField]
        List<Collider2D> colliders;
    
        [SerializeField]
        BasketMask mask;

        [SerializeField]
        Transform box;

        [SerializeField]
        GameObject starBoxObject;

        void Awake()
        {
            DeactivateStarBox();
        }

        public void Animate()
        {
            box.transform.DOPunchScale(new Vector3(0.4f, -0.3f, 0), 0.4f, 1, 2);
            var seq = DOTween.Sequence();
            seq.Append(box.transform.DOLocalMoveY(box.transform.localPosition.y - 0.6f, 0.2f))
                .Append(box.transform.DOLocalMoveY(box.transform.localPosition.y + 0.8f, 0.2f))
                .Append(box.transform.DOLocalMoveY(box.transform.localPosition.y, 0.2f));
        }

        public void SetColliders(bool isActive)
        {
            for (var i = 0; i < colliders.Count; i++)
                colliders[i].enabled = isActive;
        }

        public void DeactivateMask()
        {
            if (mask)
                mask.Deactivate();
        }

        public void ActivateMask()
        {
            if (mask)
                mask.Activate();
        }
        
        public void DeactivateStarBox()
        {
            starBoxObject.SetActive(false);
        }
        
        public void ActivateStarBox()
        {
            starBoxObject.SetActive(true);
        }
    }
}