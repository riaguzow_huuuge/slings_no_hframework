﻿using System;
using H.Games.Slings.Environment;
using H.Games.Slings.Utilities;
using UnityEngine;

namespace H.Games.Slings.Baskets
{
    [Serializable]
    public class CoinPathElement
    {
        [SerializeField]
        Vector3 localPosition;

        public CoinPathElement(Vector3 localPosition)
        {
            this.localPosition = localPosition.SetZ(0);
        }

        public Vector3 LocalPosition
        {
            get { return localPosition; }
            set { localPosition = value; }
        }
    }
}