﻿using DG.Tweening;
using H.Games.Slings.CoreModules;
using UnityEngine;

namespace H.Games.Slings.Baskets
{
    public class Blocker : MonoBehaviour
    {
        Vector3 defaultScale;
        Bounds bounds;
        BlockerType type;
    
        void Start()
        {
            Deactivate();
            defaultScale = transform.localScale;
            bounds = GetComponent<Collider2D>().bounds;
            type = transform.localRotation.eulerAngles.z == 0 ? BlockerType.Horizontal : BlockerType.Vertical;
        }

        void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Player"))
                Core.Audio.PlayBlockerBounce();
        }

        public void Hide()
        {
            var seq = DOTween.Sequence();
            seq.Append(transform.DOScale(defaultScale * 1.3f, 0.2f))
                .Append(transform.DOScale(Vector3.zero, 0.2f))
                .OnComplete(Deactivate);
        }
    
        public void Show()
        {
            Activate();
            transform.localScale = Vector3.zero;
        
            var seq = DOTween.Sequence();
            seq.Append(transform.DOScale(defaultScale * 1.3f, 0.2f))
                .Append(transform.DOScale(defaultScale, 0.2f))
                .OnComplete(UpdateBounds);
        }

        public void Deactivate()
        {
            gameObject.SetActive(false);
        }
    
        public void Activate()
        {
            gameObject.SetActive(true);
        }

        void UpdateBounds()
        {
            bounds = GetComponent<Collider2D>().bounds;
        }
        
        public Bounds GetBounds()
        {
            return bounds;
        }

        public bool IsVertical()
        {
            return type == BlockerType.Vertical;
        }
        
        public bool IsHorizontal()
        {
            return type == BlockerType.Horizontal;
        }
        
    }

    public enum BlockerType
    {
        Horizontal, Vertical
    }
}