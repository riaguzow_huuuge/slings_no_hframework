﻿using UnityEngine;

namespace H.Games.Slings.CoreModules
{
    public class BackgroundController : MonoBehaviour
    {
        static float MAX_BG_HEIGHT = 9999999999;
        [SerializeField]
        SpriteRenderer bgTile;

        public void UpdateHeight(float value)
        {
            if (bgTile.size.y < MAX_BG_HEIGHT)
                bgTile.size = new Vector2(bgTile.size.x, bgTile.size.y + value);
        }
    }
}