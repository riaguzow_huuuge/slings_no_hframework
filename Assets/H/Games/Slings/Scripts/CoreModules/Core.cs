﻿using System.Collections;
using System.Linq;
using Games.Slings.CoreModules;
using H.Games.Slings.Baskets;
using H.Games.Slings.Configs;
using H.Games.Slings.Customisation;
using H.Games.Slings.Drag;
using H.Games.Slings.Environment;
using H.Games.Slings.Player;
using H.Games.Slings.Utilities;
using HFramework.API;
using UnityEngine;

namespace H.Games.Slings.CoreModules
{
    public class Core : SlingsSingleton<Core>
    {
        [SerializeField]
        GameMaster gameMaster;

        public static GameMaster Game
        {
            get { return Instance.gameMaster; }
        }

        [SerializeField]
        Cat player;

        public static Cat Player
        {
            get { return Instance.player; }
        }

        [SerializeField]
        ScoreController scoreController;

        public static ScoreController ScoreController
        {
            get { return Instance.scoreController; }
        }

        [SerializeField]
        SwipeController swipeController;

        public static SwipeController SwipeController
        {
            get { return Instance.swipeController; }
        }

        [SerializeField]
        BasketSpawner basketSpawner;

        public static BasketSpawner BasketSpawner
        {
            get { return Instance.basketSpawner; }
        }

        [SerializeField]
        CoinSpawner coinSpawner;

        public static CoinSpawner CoinSpawner
        {
            get { return Instance.coinSpawner; }
        }

        [SerializeField]
        BounduaryController bounduary;

        public static BounduaryController Bounduary
        {
            get { return Instance.bounduary; }
        }

        [SerializeField]
        InputController inputController;

        public static InputController GameInput
        {
            get { return Instance.inputController; }
        }

        [SerializeField]
        AudioController audioController;

        public static AudioController Audio
        {
            get { return Instance.audioController; }
        }

        [SerializeField]
        UiController uiController;

        public static UiController UI
        {
            get { return Instance.uiController; }
        }

        [SerializeField]
        BackgroundController bgController;

        public static BackgroundController Background
        {
            get { return Instance.bgController; }
        }

        [SerializeField]
        SlingsCustomisation customAsset;

        public static SlingsCustomisation Customisation
        {
            get { return Instance.customAsset; }
        }

        public static SlingsDifficultyConfig Difficulty
        {
            get
            {
                return HApi.GetConfig<SlingsDifficultyConfig>();
            }
        }

        [HideInInspector]
        public bool isGameplayStarted;

        void OnEnable()
        {
            HApi.OnGameContinue += OnGameContinue;
        }

        void Start()
        {
            PrepareGame();
        }


        void PrepareGame()
        {
            ScoreController.ResetScore();
            Audio.PlayBackground();
            Audio.PlayPurring();
            UI.ActivateTutorial();
        }

#if UNITY_EDITOR || DEVELOPMENT_BUILD

        public void StopCheatScoring()
        {
            StopAllCoroutines();
        }

        public void StartCheatScoring()
        {
            StartCoroutine(CheatScoringCoroutinr());
        }

        IEnumerator CheatScoringCoroutinr()
        {
            while (true)
            {
                yield return new WaitForSecondsRealtime(1f);
                CheatScore();
            }
        }

        public void CheatScore()
        {
            var dt = FindObjectOfType<DeathTrigger>();
            if (dt)
                dt.gameObject.SetActive(false);
            var basket = FindObjectsOfType<Basket>().First(x => x != gameMaster.CurrentBasket);
            gameMaster.OnBasketScore(basket);
            Player.transform.localScale = Vector3.one;
        }
#endif
        
        public void OnGameplayStart()
        {
            isGameplayStarted = true;
            HApi.StartGame();
            GameInput.Unlock();
            Audio.StopPurring();
        }
        

        /**
         * On continue from button on result screen
         **/
        void OnGameContinue()
        {
            Game.Continue();
        }

        public void LoseGame()
        {
            if (ScoreController.CurrentScore.Equals(0))
            {
                Game.Continue();
                ScoreController.ResetScore();
                UI.ShowTutorialFirstStep();
            }
            else
            {
                HApi.LoseGame();
                Audio.PlayDeath();
                Player.Kill();
                GameInput.Lock();
            }
        }

        bool isQuitting = false;
        void OnApplicationQuit()
        {
            isQuitting = true;
        }
        void OnDisable()
        {
            if (!isQuitting)
            {
                HApi.OnGameContinue -= OnGameContinue;
            }
        }
    }
}