﻿using HFramework.GameplayUI.Currency;
using UnityEngine;

namespace H.Games.Slings.CoreModules
{
    public class UiController : MonoBehaviour
    {
        [SerializeField]
        GameObject tutorialStep1;

        [SerializeField]
        Transform tutorialStep1Place;
        
        [SerializeField]
        GameObject tutorialStep2;

        [SerializeField]
        Transform tutorialStep2Place;
        
        bool isTutorialActive = false;

        public Vector3 CoinIconPosition { get; private set; }

        void Awake()
        {
            CoinIconPosition = GetSoftCurrencyIconPosition();
            PlaceTutorial();
        }

        void Update()
        {
            PlaceTutorial();
        }

        void PlaceTutorial()
        {
            tutorialStep1.transform.position = Camera.main.WorldToScreenPoint(tutorialStep1Place.transform.position);
            tutorialStep2.transform.position = Camera.main.WorldToScreenPoint(tutorialStep2Place.transform.position);
        }

        public void HideTutorial()
        {
            HideTutorialFirstStep();
            HideTutorialSecondStep();
        }

        public void ShowTutorialFirstStep()
        {
            if (!isTutorialActive)
                return;
            tutorialStep1.gameObject.SetActive(true);
            HideTutorialSecondStep();
        }
        
        public void HideTutorialFirstStep()
        {
            tutorialStep1.gameObject.SetActive(false);
        }
        
        public void ShowTutorialSecondStep()
        {
            if (!isTutorialActive)
                return;
            tutorialStep2.gameObject.SetActive(true);
            HideTutorialFirstStep();
        }
        
        public void HideTutorialSecondStep()
        {
            tutorialStep2.gameObject.SetActive(false);
        }

        public void DeactivateTutorial()
        {
            isTutorialActive = false;
            HideTutorial();
        }

        public void ActivateTutorial()
        {
            isTutorialActive = true;
            ShowTutorialFirstStep();
        }
        
        
        Vector2 GetSoftCurrencyIconPosition()
        {
            var softCurrencyCounter = FindObjectOfType<SoftCurrencyView>();
            if (softCurrencyCounter != null)
            {
                return softCurrencyCounter.transform.position;
            }

            return new Vector2(Screen.width, Screen.height);
        }
    }
}