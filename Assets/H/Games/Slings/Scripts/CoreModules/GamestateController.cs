using System;
using H.Games.Slings.Utilities;
using HFramework.API;
using UnityEngine.Events;

namespace H.Games.Slings.CoreModules
{
    public class GamestateController : SlingsSingleton<GamestateController>
    {
        public const string BACKGROUNDS_CATEGORY = "Backgrounds";
        public const string BASKETS_CATEGORY = "Baskets";
        public const string CATS_CATEGORY = "Cats";
        
        public UnityAction OnBackgroundChanged;
        public UnityAction OnBasketChanged;
        public UnityAction OnSkinChanged;
        
        void Start()
        {
            HApi.OnCustomizationItemActivated += OnChooseItem;
        }

        public long GetSoftCurrency()
        {
            return HApi.GetSoftCurrency();
        }

        public bool TryAddSoftCurrency(int amount)
        {
            HApi.AddSoftCurrency(amount);
            return true;
        }

        public bool TryConsumeSoftCurrency(int amount)
        {
            var isConsumed = HApi.TryRemoveSoftCurrency(amount);
            return isConsumed;
        }

        public bool IsBestScore(long score)
        {
            return score > HApi.GetBestScore();
        }

        public long GetBestScore()
        {
            return HApi.GetBestScore(); 
        }

        void OnChooseItem(CustomizationID identifier)
        {
            if (identifier.categoryID == BACKGROUNDS_CATEGORY)
            {
                OnBackgroundChanged();
            }
            else if (identifier.categoryID == BASKETS_CATEGORY)
            {
                OnBasketChanged();
            }else if(identifier.categoryID == CATS_CATEGORY)
            {
                OnSkinChanged();
            }
            else
                throw new Exception(string.Format("Wrong category ({0}) or item ID ({1}).", identifier.categoryID, identifier.itemID));
        }

        bool isQuitting = false;
        void OnApplicationQuit()
        {
            isQuitting = true;
        }
        void OnDestroy()
        {
            if (!isQuitting)
            {
                HApi.OnCustomizationItemActivated -= OnChooseItem;
                OnBackgroundChanged = null;
                OnBasketChanged = null;
                OnSkinChanged = null;
            }
        }
    }
}

