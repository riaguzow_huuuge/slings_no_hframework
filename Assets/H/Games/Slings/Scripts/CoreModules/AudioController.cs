﻿using HFramework.API;
using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
using H.Games.Slings.Utilities;

namespace H.Games.Slings.CoreModules
{
    public class AudioController : MonoBehaviour
    {
        [Header("Music")]
        [SerializeField]
        AudioSource background;

        public void PlayBackground()
        {
            background.Play();
        }

        [SerializeField]
        AudioSource catPurring;

        public void PlayPurring()
        {
            catPurring.Play();
        }

        public void StopPurring()
        {
            catPurring.DOFade(0, 0.5f)
                .OnComplete(() => catPurring.Stop());
        }

        [Header("SFX")]
        [SerializeField]
        AudioClip wallBounce;

        public void PlayWallBounce()
        {
            if (IsMinVelocity)
                Play(wallBounce);
        }

        [SerializeField]
        AudioClip blockerBounce;

        public void PlayBlockerBounce()
        {
            if (IsMinVelocity)
                Play(blockerBounce);
        }

        [SerializeField]
        AudioClip boxBounce;

        public void PlayBoxBounce()
        {
            if (IsMinVelocity)
                Play(boxBounce);
        }

        [SerializeField]
        List<AudioClip> deathList;

        public void PlayDeath()
        {
            PlayRandomized(deathList);
        }

        [SerializeField]
        List<AudioClip> jumpList;

        public void PlayJump()
        {
            PlayRandomized(jumpList);
        }

        [SerializeField]
        AudioClip score;

        public void PlayScore()
        {
            var audioSource = Play(score);
            if (audioSource != null)
            {
                audioSource.volume = 0.7f;
            }
        }

        [SerializeField]
        AudioClip basketDunk;

        public void PlayBasketDunk()
        {
            Play(basketDunk);
        }

        [SerializeField]
        AudioClip higscore;

        public void PlayHighscore()
        {
            Play(higscore);
        }

        static AudioSource Play(AudioClip clip)
        {
            return HApi.PlaySfx(clip);
        }

        static AudioSource PlayRandomized(List<AudioClip> clipList)
        {
            var clip = clipList.GetRandom();
            return Play(clip);
        }

        [SerializeField]
        float minAudioVelocity;

        bool IsMinVelocity
        {
            get { return Core.Player.GetVelocity.magnitude > minAudioVelocity; }
        }
    }
}