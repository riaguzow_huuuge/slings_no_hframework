﻿using System.Linq;
using H.Games.Slings.Baskets;
using H.Games.Slings.Environment;
using UnityEngine;

namespace H.Games.Slings.CoreModules
{
    public class GameMaster : MonoBehaviour
    {
        Basket currentBasket;
        Basket nextBasket;

        public Basket CurrentBasket
        {
            get { return currentBasket; }
        }
        
        public Basket NextBasket
        {
            get { return nextBasket; }
        }

        public void SetFirstBasket(Basket newBasket)
        {
            OnBasketScore(newBasket);
        }

        public void OnBasketScore(Basket newBasket)
        {
            bool isSameBasket = newBasket == currentBasket;
            var basketDifference = GetBasketsDifference();
            if (!isSameBasket)
                ChangeBaskets(newBasket);
            
            Core.Player.SetBasketAndReset(newBasket.transform);
            
            if (!CurrentBasket.isFirst)
                Core.Audio.PlayBasketDunk();

            if (isSameBasket)
            {
                Core.ScoreController.ResetBounces();
                return;
            }

            if (!CurrentBasket.isFirst)
            {
                nextBasket = Core.BasketSpawner.SpawnNext();
                
                Core.Bounduary.MoveUp(basketDifference);
                Core.Background.UpdateHeight(basketDifference);
                Core.ScoreController.UpdateScore();
                Core.Audio.PlayScore();
                Core.UI.DeactivateTutorial();
            }
            else
            {
                nextBasket = FindObjectsOfType<Basket>().First(x => x != newBasket);
            }
        }

        float GetBasketsDifference()
        {
            return currentBasket && nextBasket ? nextBasket.transform.position.y - currentBasket.transform.position.y : 0;
        }

        void ChangeBaskets(Basket newBasket)
        {
            if (currentBasket != null)
                currentBasket.Drop();
            currentBasket = newBasket;
        }
        

        public void Continue()
        {
            currentBasket.SetCollidersActive(false);
            currentBasket.ResetRotation();
            Core.Player.SetBasketAndReset(currentBasket.transform);
            Core.Player.isBallThrown = false;
            Core.ScoreController.ResetBounces();
            RefreshWalls();
        }

        void RefreshWalls()
        {
            var walls = FindObjectsOfType<Wall>();
            for (int i = 0; i < walls.Length; i++)
                walls[i].Refresh();
        }
    }
}