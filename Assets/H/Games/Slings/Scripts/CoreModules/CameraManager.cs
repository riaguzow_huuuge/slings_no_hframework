﻿using Cinemachine;
using UnityEngine;

namespace H.Games.Slings.CoreModules
{
	public class CameraManager : MonoBehaviour
	{
		public const float DEFAULT_SIZE = 50f;
		private const float DEFAULT_RATIO = 9 / 16f;
		private const float DEFAULT_WIDTH = DEFAULT_SIZE * DEFAULT_RATIO;

		void Awake()
		{
			Setup();
		}
	
		void Setup()
		{
			var vCam = GetComponentInChildren<CinemachineVirtualCamera>();
			if (Camera.main.aspect < DEFAULT_RATIO)
			{
				vCam.m_Lens.OrthographicSize
					= DEFAULT_WIDTH / Camera.main.aspect;
				Camera.main.orthographicSize = vCam.m_Lens.OrthographicSize;
			}
			GetComponentInChildren<CinemachineVirtualCamera>().m_Lens = vCam.m_Lens;	
		}
	}
}