﻿using HFramework.API;
using H.Games.Slings.Environment;
using UnityEngine;
using UnityEngine.Events;

namespace H.Games.Slings.CoreModules
{
    public class ScoreController : MonoBehaviour
    {
        public UnityEvent OnHighscoreBeatenEvent;
        
        [SerializeField]
        ScorePoint pointPrefab;

        int currentScore;

        public int CurrentScore
        {
            get { return currentScore; }
        }

        int bounceCounter;
        int clearShotCounter;

        public void ResetScore()
        {
            currentScore = 0;
            clearShotCounter = 0;
            ResetBounces();
        }

        public void UpdateScore()
        {
            AddPoint();
            AddComboPoints();
            ShowCombo(Core.Game.CurrentBasket.transform.position);
            ResetBounces();
            TryToTriggerHighscore();
            HApi.SetCurrentScore(CurrentScore);
        }

        void AddPoint()
        {
            currentScore++;
        }

        void AddComboPoints()
        {
            currentScore += GetComboPoints();
        }

        int GetComboPoints()
        {
            return bounceCounter + clearShotCounter;
        }

        public void AddWallBounce()
        {
            bounceCounter++;
        }

        public void CountClearShot(bool isClear)
        {
            if (isClear)
                clearShotCounter++;
            else
                clearShotCounter = 0;
        }

        public void ResetBounces()
        {
            bounceCounter = 0;
        }

        void TryToTriggerHighscore()
        {
            if (GamestateController.Instance.IsBestScore(currentScore) && GamestateController.Instance.GetBestScore() > 0)
                OnHighscoreBeatenEvent.Invoke();
        }
        
        void ShowCombo(Vector2 position)
        {
            ShowPoints(position, 1 + GetComboPoints());

            if (clearShotCounter > 0)
                ShowClearShot(position, clearShotCounter);
            
            if (bounceCounter > 0)
                ShowBounce(position, clearShotCounter > 0);

        }

        void ShowPoints(Vector2 pos, int pts)
        {
            SpawnText(pos).SetPoints(pts);
        }

        void ShowBounce(Vector2 pos, bool isSecond)
        {
            SpawnText(pos).SetBounce(0, isSecond);
        }

        void ShowClearShot(Vector2 pos, int pts)
        {
            SpawnText(pos).SetClearShot(pts);
        }

        ScorePoint SpawnText(Vector2 position)
        {
            return Instantiate(pointPrefab.gameObject, position, Quaternion.identity).GetComponent<ScorePoint>();
        }
    }
}