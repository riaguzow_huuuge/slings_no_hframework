﻿using System.Collections;
using H.Games.Slings.CoreModules;
using H.Games.Slings.Utilities;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Games.Slings.CoreModules
{
    public class InputController : MonoBehaviour, IPointerDownHandler
    {
        bool isBlocked;
        Coroutine inputCoroutine;

        void Awake()
        {
            Input.multiTouchEnabled = true;
            var pauseListener = gameObject.GetOrAddComponent<PauseListener>();
            pauseListener.AddPauseAction(OnGamePaused);
        }
    
        void OnGamePaused(bool isPaused)
        {
            isBlocked = isPaused;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (inputCoroutine == null)
            {
                inputCoroutine = StartCoroutine(ProcessInput());
            }
        }

        IEnumerator ProcessInput()
        {
            var isButtonDown = Input.GetMouseButtonDown(0);
            var isButton = Input.GetMouseButton(0);
            var isButtonUp = Input.GetMouseButtonUp(0);

            while (isButtonDown || isButton || isButtonUp)
            {
                if (isBlocked)
                {
                    yield break;
                }

#if UNITY_EDITOR
                if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(1))
                {
                    Core.Instance.CheatScore();
                    yield break;
                }
#endif
        
#if UNITY_EDITOR || DEVELOPMENT_BUILD
                if (isCheatMode)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        Core.Instance.CheatScore();
                    }
                    yield break;
                }
#endif

                if (!Core.Instance.isGameplayStarted &&
                    (isButtonDown || isButton || isButtonUp) )
                {
                    Core.Instance.OnGameplayStart();
                }
                if (isButtonDown)
                    Core.SwipeController.StartDrag(CalculateMousePos());
                else if (isButton)
                    Core.SwipeController.UpdateDrag(CalculateMousePos());
                else if (isButtonUp)
                    Core.SwipeController.EndDrag(CalculateMousePos());
            
                yield return null;
                isButtonDown = Input.GetMouseButtonDown(0);
                isButton = Input.GetMouseButton(0);
                isButtonUp = Input.GetMouseButtonUp(0);
            }

            inputCoroutine = null;
        }

        static Vector3 CalculateMousePos()
        {
            var mousePosition = GetInputPosition();
            mousePosition.z = Mathf.Abs(Camera.main.transform.position.z);
            return Camera.main.ScreenToWorldPoint(mousePosition);
        }

        static Vector3 GetInputPosition()
        {
#if UNITY_EDITOR
            return Input.mousePosition;
#else
        return Input.touches[0].position;
#endif
        }
    
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        public bool isCheatMode;

#endif
    
        public void Unlock()
        {
            isBlocked = false;
        }

        public void Lock()
        {
            isBlocked = true;
        }
    }
}