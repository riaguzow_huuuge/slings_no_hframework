﻿using UnityEngine;

namespace H.Games.Slings.CoreModules
{
    public class BounduaryController : MonoBehaviour
    {
        public float LeftGameplayBound;
        public float RightGameplayBound;
        public float GameplayWidth;

        public float LeftScreenBound;
        public float RightScreenBound;

        [Header("Camera Area")]
        [SerializeField]
        PolygonCollider2D camAreaTrigger;

        [SerializeField]
        float bottomOffset = -5;

        [SerializeField]
        float upperOffset = 100;

        [Header("Dead Trigger")]
        [SerializeField]
        BoxCollider2D deadTrigger;

        Vector3 CameraSizeOffset
        {
            get{return new Vector2(0, Camera.main.orthographicSize - CameraManager.DEFAULT_SIZE);}
        }

        void Start()
        {
            var leftBotom = Camera.main.ScreenToWorldPoint(new Vector2(0, 0));
            var rightUpper = Camera.main.ScreenToWorldPoint(new Vector2(Camera.main.pixelWidth, Camera.main.pixelHeight));
            var screenSize = rightUpper - leftBotom;

            LeftScreenBound = leftBotom.x;
            RightScreenBound = rightUpper.x;
            LeftGameplayBound =  -25.5f;
            RightGameplayBound = 25.5f;

            GameplayWidth = RightGameplayBound-LeftGameplayBound;

            SetUpCameraArea(leftBotom, screenSize.x, screenSize.y);
            SetUpDeadTrigger(leftBotom);
            transform.position += CameraSizeOffset;
        }

        void SetUpCameraArea(Vector2 lBottom, float width, float height)
        {
            Vector2 rBottom = lBottom + new Vector2(width, 0) + new Vector2(0, bottomOffset);
            Vector2 lUpper = lBottom + new Vector2(0, height) + new Vector2(0, upperOffset);
            Vector2 rUpper = lBottom + new Vector2(width, height) + new Vector2(0, upperOffset);
            lBottom += new Vector2(0, bottomOffset);


            Vector2[] pList = {lBottom, rBottom, rUpper, lUpper};

            camAreaTrigger.points = pList;
        }

        void SetUpDeadTrigger(Vector2 lBottom)
        {
            deadTrigger.transform.position = new Vector3(0, lBottom.y + bottomOffset - deadTrigger.size.y, 0);
        }

        bool first = true;
        public void MoveUp(float yMovement)
        {
            if (first)
            {
                yMovement -= 15;
                first = false;
            }
            var transformPosition = transform.position;
            transformPosition.y += yMovement;
            transform.position = transformPosition;
        }

        public static Vector2 GetRatioVector(float ratioFactor)
        {
            var leftBottomPt = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
            var maxVector = Camera.main.ScreenToWorldPoint(
                new Vector3(Screen.width / ratioFactor, Screen.height / ratioFactor, 0));
            return maxVector - leftBottomPt;
        }
    }
}