﻿using System.Collections;
using DG.Tweening;
using H.Games.Slings.CoreModules;
using H.Games.Slings.Utilities;
using UnityEngine;

namespace H.Games.Slings.Player
{
    public class CatRig : MonoBehaviour
    {
        [SerializeField]
        Animator catAnimator;

        float PullStep
        {
            get { return catAnimator.GetFloat("Position"); }
            set { catAnimator.SetFloat("Position", value); }
        }

        float Velocity
        {
            get { return catAnimator.GetFloat("Velocity"); }
            set { catAnimator.SetFloat("Velocity", value); }
        }

        float Rotation
        {
            get { return catAnimator.GetFloat("Rotation"); }
            set { catAnimator.SetFloat("Rotation", value); }
        }

        bool IsTailRolled
        {
            get { return catAnimator.GetBool("isTailRolled"); }
            set { catAnimator.SetBool("isTailRolled", value); }
        }
    
        void Awake()
        {
            var pauseListener = gameObject.GetOrAddComponent<PauseListener>();
            pauseListener.AddPauseAction(OnGamePaused);
            IsTailRolled = true;
        }

        void OnGamePaused(bool isPaused)
        {
            if (isPaused)
            {
                ResetRig(() => catAnimator.enabled = false);

            }
            else
                catAnimator.enabled = true;
        }

        public void SetPullStep(float step)
        {
            PullStep = step;
            IsTailRolled = false;
        }

        public void ResetRig(TweenCallback tweenCallback = null)
        {
            ResetPull(PullStep * 1, tweenCallback);
        }

        public void MoveToIdle(TweenCallback tweenCallback = null)
        {
            ResetPull(0.1f, tweenCallback);
        }

        void ResetPull(float time, TweenCallback tweenCallback = null)
        {
            var posTween = DOTween.To(() => PullStep, x => PullStep = x, 0, time);
            if (tweenCallback != null)
                posTween.OnComplete(tweenCallback);
        }
    
        bool blockBall;
        public void TriggerBallState()
        {
            if (blockBall)
                return;
            catAnimator.SetTrigger("CatBall");
            StartCoroutine(BlockBall());
        }

        IEnumerator BlockBall()
        {
            blockBall = true;
            yield return new WaitForSecondsRealtime(0.3f);
            blockBall = false;
        }

        public void SetRotation(float rot)
        {
            Rotation = rot;
        }

        public void TriggerFlyState()
        {

            catAnimator.SetTrigger("Fly");
        }
    
        public void TriggerJump()
        {

            catAnimator.SetTrigger("doJump");
        }


        float targetVelocity = 0;

        public void SetVelocity(float v)
        {
            var newVelocity = Mathf.Sign(v);
            if (targetVelocity != newVelocity)
            {
                targetVelocity = newVelocity;
                ChangeVelocity();
            }
        }

        void ChangeVelocity()
        {
            DOTween.To(() => Velocity, x => Velocity = x, targetVelocity, 0.5f);
        }

        public void HideBasketMask()
        {
            Core.Game.CurrentBasket.SetPullingState();
        }

        public void UnlockInput()
        {
            Core.Player.isBallThrown = false;
            //Core.GameInput.Unlock();
        }
    }
}