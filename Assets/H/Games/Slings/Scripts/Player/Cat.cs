﻿using System;
using System.Collections;
using DG.Tweening;
using H.Games.Slings.CoreModules;
using H.Games.Slings.Customisation;
using H.Games.Slings.Utilities;
using UnityEngine;

namespace H.Games.Slings.Player
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class Cat : MonoBehaviour
    {
        [SerializeField]
        Rigidbody2D catRigidbody;

        public bool isBallThrown = false;

        [SerializeField]
        CatRig rigprefab;

        CatRig rig;

        [SerializeField]
        LayingCat waitingcat;

        [SerializeField]
        Transform rigContainer;


        public GameObject boxParts;

        void Awake()
        {
            catRigidbody.gravityScale = 0;
        }

        void Start()
        {
            transform.localPosition = Vector3.zero;
        }

        void OnCollisionEnter2D(Collision2D other)
        {
            rig.TriggerBallState();
        }

        public void Stretch(float ratio)
        {
            rig.SetPullStep(ratio);
        }

        public void StartStretch()
        {
            try
            {
                if (rig == null)
                    return;
                rig.gameObject.SetActive(true);
                waitingcat.Deactivate();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public void SetBasketAndReset(Transform basket)
        {
            SetBasket(basket);
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            catRigidbody.freezeRotation = true;
            DestroyRig();
        }

        void DestroyRig()
        {
            if (Core.Game.CurrentBasket.isFirst)
            {
                waitingcat.transform.localScale = Vector3.zero;
                waitingcat.transform.DOScale(Vector3.one, 0.1f);
                if (Core.SwipeController.IsDraging)
                    waitingcat.Deactivate();
                else
                    waitingcat.Activate();
            }
            SetUpNewRig();
            SetInBasketState();
            StopAllCoroutines();
        }

        void SetUpNewRig()
        {
            if (rig != null)
                Destroy(rig.gameObject);

            rig = Instantiate(rigprefab, rigContainer);
            rig.transform.localPosition = Vector3.zero;
            rig.transform.localRotation = Quaternion.identity;
        
            if (Core.Game.CurrentBasket.isFirst && !Core.SwipeController.IsDraging)
                rig.gameObject.SetActive(false);
            if (!Core.Game.CurrentBasket.isFirst)
                rig.TriggerJump();
        }

        void SetInBasketState()
        {
            catRigidbody.gravityScale = 0;
            catRigidbody.velocity = Vector2.zero;
            catRigidbody.isKinematic = true;
            //isBallThrown = false;
            StopRotation();
        }

        void SetBasket(Transform t)
        {
            transform.SetParent(t);
        }

        public void Throw(Vector3 force)
        {
            catRigidbody.freezeRotation = false;
            catRigidbody.isKinematic = false;

            catRigidbody.gravityScale = 1;
            catRigidbody.AddForce(force, ForceMode2D.Impulse);
            isBallThrown = true;
            SetBasket(null);
            StartCoroutine(ActivateCollidersInSec());
            StartRotation();
            rig.ResetRig();
            rig.TriggerFlyState();
        }

        Coroutine rotation;

        void StartRotation()
        {
            rotation = StartCoroutine(Rotate());
        }

        void StopRotation()
        {
            if (rotation != null)
                StopCoroutine(rotation);
            transform.rotation = Core.Game.CurrentBasket.transform.rotation;
        }

        Quaternion rot;

        IEnumerator Rotate()
        {
            while (true)
            {
                var angle = SlingsUtils.GetAngleByVector(catRigidbody.velocity);
                rot = Quaternion.AngleAxis(angle, Vector3.forward);
                transform.rotation = Quaternion.Lerp(transform.rotation, rot, Time.deltaTime * 20);

                rig.SetRotation(angle);

                rig.SetVelocity(catRigidbody.velocity.y);
                yield return null;
            }
        }


        IEnumerator ActivateCollidersInSec()
        {
            yield return new WaitForSeconds(0.2f);
            Core.Game.CurrentBasket.ActivateColliders();
        }

        public void ResetToZero()
        {
            rig.ResetRig();
        }

        public Vector2 GetVelocity
        {
            get { return catRigidbody.velocity; }
        }

        public void AddForce(Vector2 force)
        {
            catRigidbody.AddForce(force, ForceMode2D.Impulse);
        }

        public void Kill()
        {
            if (rig != null)
                Destroy(rig.gameObject);
            
            SetInBasketState();
            StopAllCoroutines();
        }
    }
}