﻿using H.Games.Slings.Utilities;
using UnityEngine;

namespace H.Games.Slings.Player
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class Rigidbody2DPauser : MonoBehaviour
    {
        Rigidbody2D rb;
    
        Vector3 savedVelocity;
        float savedAngularVelocity;
        float savedAngularDrag;
    
        void Awake()
        {
            rb = GetComponent<Rigidbody2D>();
            var pauseListener = gameObject.GetOrAddComponent<PauseListener>();
            pauseListener.AddPauseAction(OnGamePaused);
        }
    
        void OnGamePaused(bool isPaused)
        {
            if (isPaused)
            {
                savedVelocity = rb.velocity;
                savedAngularVelocity = rb.angularVelocity;
                savedAngularVelocity = rb.angularDrag;
                rb.isKinematic = true;
                rb.simulated = false;
            }
            else
            {
                rb.isKinematic = false;
                rb.simulated = true;
                rb.AddForce( savedVelocity, ForceMode2D.Force );
                rb.AddTorque( savedAngularVelocity, ForceMode2D.Force );
                rb.angularDrag = savedAngularVelocity;
            }
        }
    }
}