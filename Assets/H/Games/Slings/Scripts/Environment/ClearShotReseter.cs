﻿using H.Games.Slings.Baskets;
using H.Games.Slings.CoreModules;
using UnityEngine;

namespace H.Games.Slings.Environment
{
    public class ClearShotReseter : MonoBehaviour
    {
        float lastHitTime;
        void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                GetComponentInParent<Basket>().ResetClearShot();
                if (Time.time - lastHitTime > 0.35f)
                    Core.Audio.PlayBoxBounce();
                lastHitTime = Time.time;
            }
        }
    }
}