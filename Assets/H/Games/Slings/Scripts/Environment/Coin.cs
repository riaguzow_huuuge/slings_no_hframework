﻿using DG.Tweening;
using System.Collections;
using H.Games.Slings.CoreModules;
using UnityEngine;

namespace H.Games.Slings.Environment
{
    public class Coin : MonoBehaviour
    {
        [SerializeField]
        Collider2D trigger;

        Vector3 CoinIconPosition
        {
            get { return Camera.main.ScreenToWorldPoint(Core.UI.CoinIconPosition); }
        }

        bool isCollecting = false;
        
        void Start()
        {
            transform.localScale = Vector3.zero;
            transform.DOScale(Vector3.one, 0.3f);
            var seq = DOTween.Sequence();
            seq.Append(transform.DOBlendableLocalMoveBy(new Vector3(0, 2), 3));
            seq.SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            Collect();
        }

        public void Collect(float delay = 0)
        {
            DeactivateTrigger();
            StartCoroutine(Collector(delay));
            isCollecting = true;
        }

        float timeToFinish = 0.7f;

        IEnumerator Collector(float delay = 0)
        {
            yield return new WaitForSeconds(delay);
            
            float t = 0;
            transform.DOScaleX(0, 1f).SetEase(Ease.InFlash, 10, 0);
            
            while (true)
            {
                if (t < timeToFinish )
                {
                    t += Time.deltaTime;
                    transform.position = Vector3.Lerp(transform.position, CoinIconPosition, t/timeToFinish);
                }
                else
                {
                    GamestateController.Instance.TryAddSoftCurrency(1);
                    Destroy(gameObject);
                    yield break;
                }

                yield return null;
            }
        }

        public void DeactivateTrigger()
        {
            trigger.enabled = false;
        }

        public void Hide()
        {
            transform.DOScale(Vector3.zero, 0.4f).OnComplete(Destroy);
        }

        void Destroy()
        {
            if (!isCollecting)
                Destroy(gameObject);
        }
    }
}