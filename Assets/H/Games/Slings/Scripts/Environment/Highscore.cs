﻿using System.Collections.Generic;
using H.Games.Slings.CoreModules;
using H.Games.Slings.Utilities;
using UnityEngine;

namespace H.Games.Slings.Environment
{
    public class Highscore : MonoBehaviour
    {
        [SerializeField]
        List<GameObject> particles;

        void OnEnable()
        {
            Core.ScoreController.OnHighscoreBeatenEvent.AddListener(ShowParticles);
        }

        void RemoveListener()
        {
            Core.ScoreController.OnHighscoreBeatenEvent.RemoveListener(ShowParticles);
        }

        void Start()
        {
            particles[0].SetActive(false);
            particles[1].SetActive(false);
            particles[0].transform.position = particles[0].transform.position.SetX(Core.Bounduary.LeftGameplayBound);
            particles[1].transform.position = particles[1].transform.position.SetX(Core.Bounduary.RightGameplayBound);
        }

        void ShowParticles()
        {
            for (int i = 0; i < particles.Count; i++)
                particles[i].SetActive(true);

            RemoveListener();
            Core.Audio.PlayHighscore();
        }
    }
}