﻿using H.Games.Slings.CoreModules;
using UnityEngine;

namespace H.Games.Slings.Environment
{
    [RequireComponent(typeof(Collider2D))]
    public class DeathTrigger : MonoBehaviour
    {
        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
                Core.Instance.LoseGame();
        }
    }
}