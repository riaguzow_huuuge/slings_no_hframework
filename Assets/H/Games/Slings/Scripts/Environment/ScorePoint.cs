﻿using DG.Tweening;
using H.Games.Slings.CoreModules;
using H.Games.Slings.Utilities;
using UnityEngine;

namespace H.Games.Slings.Environment
{
    public class ScorePoint : MonoBehaviour
    {
        [SerializeField]
        TextMesh textComponent;

        [SerializeField]
        MeshRenderer meshRenderer;
    
        int height = 35;
        float alignOffset = 2;
    
        void SetUp()
        {
            textComponent = GetComponent<TextMesh>();

        }

        public void SetPoints(int pointsCount)
        {
            SetUp();
            textComponent.text = string.Format("+{0}", pointsCount);
            AnimatePoint();
        }
    
        void AnimatePoint()
        {        
            textComponent.color = textComponent.color.SetAlpha(0);
            var h = height - 0;
            Animate(h, 0f, 0.6f, 0.1f, 0.5f);
        }

        public void SetClearShot(int modifier)
        {
            SetUp();
            textComponent.text = string.Format("CLEAR SHOT x{0}", modifier);
            textComponent.fontSize /= 3;
            AnimateAdditional(false);
        }

        public void SetBounce(int modifier, bool isSecond)
        {
            SetUp();
            textComponent.text = string.Format("OFF THE WALL!");
            textComponent.fontSize /= 3;
            AnimateAdditional(isSecond);
        }
    
        void AnimateAdditional(bool isSecond)
        {
            textComponent.color = textComponent.color.SetAlpha(0);
            if (isSecond)
            {
                var h = height - 8.5f;
                Animate(h, 0.2f, 0.4f, 0.1f, 0.5f);
            }
            else
            {
                var h = height - 5;
                Animate(h, 0.1f, 0.5f, 0.1f, 0.5f);
            }
        }
    
        void Animate(float height, float showDelay, float hideDelay, float showDuration, float hideDuration)
        {
            var seq = DOTween.Sequence();
            seq.Append(transform.DOMoveY(transform.position.y + height, 0.4f))
                .Join(DOTween.ToAlpha(() => textComponent.color, x => textComponent.color = x, 1f, showDuration))
                .Append(DOTween.ToAlpha(() => textComponent.color, x => textComponent.color = x, 0f, hideDuration).SetDelay(hideDelay))
                .SetDelay(showDelay).OnComplete(Destroy).OnUpdate(Align);
        }

        float rAlign;
        float lAlign;
        void Align()
        {
            rAlign = meshRenderer.bounds.max.x - Core.Bounduary.RightScreenBound + alignOffset;
            lAlign = meshRenderer.bounds.min.x - Core.Bounduary.LeftScreenBound - alignOffset;

            if (rAlign > 0)
                transform.position = transform.position - new Vector3(rAlign, 0, 0);
        
            if (lAlign < 0)
                transform.position = transform.position - new Vector3(lAlign, 0, 0);
        }

        void Destroy()
        {
            Destroy(gameObject);
        }
    }
}