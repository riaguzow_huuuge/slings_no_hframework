﻿using System.Collections;
using H.Games.Slings.CoreModules;
using UnityEngine;

namespace H.Games.Slings.Environment
{
    public class Wall : MonoBehaviour
    {
        [SerializeField]
        GameObject hitparticles;

        float lastHitTime;
        int hits;
        bool IsGlitch
        {
            get { return hits > 3; }
        }

        Collider2D wallCollider;

        void Awake()
        {
            wallCollider = GetComponent<Collider2D>();
        }

        public void Refresh()
        {
            StopAllCoroutines();
            wallCollider.enabled = true;
            collisionStayTime = 0;
        }
    
        void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                if (IsGlitch)
                {
                    StartCoroutine(TurnWallOffForSeconds());
                    return;
                }

                Core.ScoreController.AddWallBounce();
                SpawnParticles(other);
                if (Time.time - lastHitTime > 0.35f)
                    Core.Audio.PlayWallBounce();

                ManageHit();
            }
        }

        void SpawnParticles(Collision2D collision)
        {
            var position = collision.contacts[0].point;
            var rotation = new Vector3(0, 0, 90) * -Mathf.Sign(collision.relativeVelocity.x);
            Instantiate(hitparticles, position, Quaternion.Euler(rotation));
        }
    
        void ManageHit()
        {
            lastHitTime = Time.time;
            hits++;
            StopAllCoroutines();
            StartCoroutine(Wait());
        }

        IEnumerator Wait()
        {
            yield return new WaitForSecondsRealtime(0.3f);
            hits = 0;
        }

        IEnumerator TurnWallOffForSeconds()
        {
            wallCollider.enabled = false;
            yield return new WaitForSecondsRealtime(4);
            wallCollider.enabled = true;
        }
    
        float collisionStayTime;
        void OnCollisionStay2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                if (collisionStayTime>1f)
                    StartCoroutine(TurnWallOffForSeconds());
                else
                    collisionStayTime += Time.deltaTime;
            }
        }

        void OnCollisionExit2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Player"))
                collisionStayTime = 0;
        }
    }
}