﻿using UnityEngine;

namespace HFramework.Loader
{
    public class SplashScreen : MonoBehaviour
    {
        [SerializeField] string hidingTriggerName = "Hide";

        public void Hide()
        {
            var animator = GetComponent<Animator>();
            if (animator != null)
            {
                animator.SetTrigger(hidingTriggerName);
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}