﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace HFramework.Loader
{
    public class LoadHFramework : MonoBehaviour
    {
        SplashScreen splashScreen;
        
        void Awake()
        {
            SceneManager.sceneLoaded += OnLoadScene;
        }
        
        void Start()
        {
            splashScreen = FindObjectOfType<SplashScreen>();
            SceneManager.LoadSceneAsync(SceneName.GAMEPLAY_UI);
        }

        void OnLoadScene(Scene scene, LoadSceneMode mode)
        {
            switch (scene.name)
            {
                // Dummy scene with only EventSystem (in real framework there are buttons there and such)
                case SceneName.GAMEPLAY_UI:
                    SceneManager.LoadSceneAsync(SceneName.GAMEPLAY, LoadSceneMode.Additive);
                    break;
                case SceneName.GAMEPLAY:
                    TryHideSplashScreen();
                    RemoveLoader();
                    break;
            }
        }

        void TryHideSplashScreen()
        {
            if(splashScreen != null)
                splashScreen.Hide();
        }

        void RemoveLoader()
        {
            Destroy(gameObject);
        }

        void OnDestroy()
        {
            SceneManager.sceneLoaded -= OnLoadScene;
        }
    }
}
