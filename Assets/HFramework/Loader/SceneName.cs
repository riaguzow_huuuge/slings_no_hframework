﻿namespace HFramework.Loader
{
    public static class SceneName
    {
        public const string GAMEPLAY = "Gameplay";
        public const string GAMEPLAY_UI = "GameplayUI";
    }
}