﻿using UnityEngine;

namespace HFramework.Utils.Effects
{
    public class Rotation : MonoBehaviour
    {
        [SerializeField] float speed = 400f;

        void Update()
        {
            GetComponent<RectTransform>().Rotate(Vector3.forward, -Time.unscaledDeltaTime * speed);
        }
    }
}