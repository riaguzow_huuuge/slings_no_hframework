﻿using DG.Tweening;
using UnityEngine;

namespace HFramework.Utils.Effects
{
    [RequireComponent(typeof(CanvasGroup))]
    public class FadeInGroup : MonoBehaviour
    {
        [SerializeField] float delay;
        [SerializeField] float duration;
        
        void Start()
        {
            var canvasGroup = GetComponent<CanvasGroup>();
            canvasGroup.alpha = 0.0f;
            canvasGroup
                .DOFade(1.0f, duration)
                .SetDelay(delay)
                .SetUpdate(true);
        }
    }
}