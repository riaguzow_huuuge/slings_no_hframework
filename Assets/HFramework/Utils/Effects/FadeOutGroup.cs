﻿using DG.Tweening;
using UnityEngine;

namespace HFramework.Utils.Effects
{
    [RequireComponent(typeof(CanvasGroup))]
    public class FadeOutGroup : MonoBehaviour
    {
        [SerializeField] float delay;
        [SerializeField] float duration;
        [SerializeField] bool shouldDestroy;

        void Start()
        {
            var canvasGroup = GetComponent<CanvasGroup>();
            var tween = canvasGroup
                .DOFade(0.0f, duration)
                .SetDelay(delay)
                .SetUpdate(true);
            if (shouldDestroy)
                tween.OnComplete(() => Destroy(gameObject));
        }
    }
}