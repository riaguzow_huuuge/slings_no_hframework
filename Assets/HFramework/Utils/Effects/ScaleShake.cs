﻿using DG.Tweening;
using UnityEngine;

namespace HFramework.Utils.Effects
{
    public class ScaleShake : MonoBehaviour
    {
        [SerializeField] Vector3 value;
        [SerializeField] float duration;
        [SerializeField] float delay;
        [SerializeField] int counts = 1;

        Sequence sequence;

        void OnEnable()
        {
            if (counts > 0)
                PlayShake();
        }

        void PlayShake()
        {
            sequence = DOTween.Sequence();
            sequence.Append(transform.DOShakeScale(duration, value).SetDelay(delay).SetLoops(counts));
            sequence.SetLoops(int.MaxValue);
            sequence.Play();
        }

        void OnDisable()
        {
            if (sequence != null)
            {
                sequence.Kill();
                sequence = null;
            }
        }
    }
}