﻿using UnityEngine;

namespace HFramework.Utils.Animators
{
    public class DestroyOnFinishState : StateMachineBehaviour
    {
        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            Destroy(animator.gameObject);
        }
    }
}