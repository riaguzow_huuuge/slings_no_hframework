﻿using UnityEditor;

namespace HFramework.Utils.PlayerPrefs.Editor
{
    public class ClearPlayerPrefs : EditorWindow
    {
        [MenuItem("HFramework/Clear Player Prefs")]
        static void Init()
        {
            HPlayerPrefs.DeleteAll();
        }
    }
}