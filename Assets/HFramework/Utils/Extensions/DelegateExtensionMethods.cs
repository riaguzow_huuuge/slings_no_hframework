﻿namespace HFramework.Utils.Extensions
{
    public static class DelegateExtensionMethods
    {
        public static bool IsValid(this System.Delegate del)
        {
            return del != null && (del.Method.IsStatic || del.Target != null && !del.Target.Equals(null));
        }
    }
}