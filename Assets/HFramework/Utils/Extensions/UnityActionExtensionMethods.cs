﻿using UnityEngine.Events;

namespace HFramework.Utils.Extensions
{
    public static class UnityActionExtensionMethods
    {
        public static bool Dispatch(this UnityAction action)
        {
            if (action.IsValid())
            {
                action();
                return true;
            }

            return false;
        }

        public static bool Dispatch<T>(this UnityAction<T> action, T parameter)
        {
            if (action.IsValid())
            {
                action(parameter);
                return true;
            }

            return false;
        }

        public static bool Dispatch<T, U>(this UnityAction<T, U> action, T parameter1, U parameter2)
        {
            if (action.IsValid())
            {
                action(parameter1, parameter2);
                return true;
            }

            return false;
        }

        
    }
}