﻿using UnityEngine;

namespace HFramework.Utils.Extensions
{
    public static class ColorExtensions
    {
        public static Color FromAlpha(this Color color, float alpha)
        {
            return new Color(
                color.r,
                color.g,
                color.b,
                alpha
            );
        }
    }
}