﻿using System.Linq;
using UnityEngine;

namespace HFramework.Utils
{
    public static class GameName
    {
        const string UNKNOWN_GAME_NAME = "Unknown";

        public static string FromBundleID
        {
            get
            {
                var bundleId = Application.identifier;
                var gameName = bundleId.Split('.').Last();

                return !string.IsNullOrEmpty(gameName) ? gameName : UNKNOWN_GAME_NAME;
            }
        }
    }
}