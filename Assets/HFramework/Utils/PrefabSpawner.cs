﻿using UnityEngine;

namespace HFramework.Utils
{
    [System.Serializable]
    public class PrefabSpawner<T> where T : MonoBehaviour
    {
        [SerializeField] T prefab;
        [SerializeField] Transform parent;
        T instance;

        public T GetInstance()
        {
            Init();
            return instance;
        }

        public void Init()
        {
            if (instance == null)
                instance = Object.Instantiate(prefab, parent);
        }

        public bool IsInstantiated()
        {
            return instance != null;
        }
        
        public static implicit operator T(PrefabSpawner<T> spawner)
        {
            return spawner.GetInstance();
        }
    }
}