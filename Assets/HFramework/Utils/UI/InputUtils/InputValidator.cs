﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace HFramework.Utils.UI.InputUtils
{
    [RequireComponent(typeof(InputField))]
    public class InputValidator : MonoBehaviour
    {
        [SerializeField] bool allowLetters;
        [SerializeField] bool allowDigits;
        [SerializeField] bool allowWhiteSpaces;
        [SerializeField] List<char> permittedChars;

        void Awake()
        {
            var inputField = GetComponent<InputField>();
            inputField.onValidateInput += OnValidateCharacter;
        }

        char OnValidateCharacter(string text, int charIndex, char addedChar)
        {
            if (
                ValidateLetters(addedChar) ||
                ValidateDigits(addedChar) ||
                ValidateWhiteSpaces(addedChar) ||
                ValidateAdditionalChars(addedChar))
            {
                return addedChar;
            }

            return '\0';
        }

        bool ValidateLetters(char addedChar)
        {
            return allowLetters && char.IsLetter(addedChar);
        }

        bool ValidateDigits(char addedChar)
        {
            return allowDigits && char.IsDigit(addedChar);
        }

        bool ValidateWhiteSpaces(char addedChar)
        {
            return allowWhiteSpaces && char.IsWhiteSpace(addedChar);
        }

        bool ValidateAdditionalChars(char addedChar)
        {
            if (permittedChars.Count == 0)
            {
                return false;
            }
            else
            {
                return permittedChars.Contains(addedChar);
            }
        }
    }
}