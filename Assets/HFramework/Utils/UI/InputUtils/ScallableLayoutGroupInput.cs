﻿using UnityEngine;
using UnityEngine.UI;

namespace HFramework.Utils.UI.InputUtils
{
    [RequireComponent(typeof(InputField))]
    public class ScallableLayoutGroupInput : MonoBehaviour
    {
        [SerializeField] LayoutGroup layoutGroup;

        void Start()
        {
            TryFindLayoutGroup();
            AddListenerToInput();
        }

        void TryFindLayoutGroup()
        {
            if (layoutGroup == null)
                layoutGroup = GetComponentInParent<HorizontalLayoutGroup>();
        }

        void AddListenerToInput()
        {
            var inputField = GetComponent<InputField>();
            inputField.onValueChanged.AddListener(RefreshLayoutGroup);
        }

        void RefreshLayoutGroup(string value)
        {
            layoutGroup.enabled = false;
            layoutGroup.enabled = true;
        }
    }
}
