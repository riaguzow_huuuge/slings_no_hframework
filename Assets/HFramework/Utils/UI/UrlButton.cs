﻿using UnityEngine;
using UnityEngine.UI;

namespace HFramework.Utils.UI
{
    [RequireComponent(typeof(Button))]
    public class UrlButton : MonoBehaviour
    {
        [SerializeField] string url;

        void Awake()
        {
            var button = GetComponent<Button>();
            button.onClick.AddListener(OpenUrl);
        }

        void OpenUrl()
        {
            Application.OpenURL(url);
        }
    }
}