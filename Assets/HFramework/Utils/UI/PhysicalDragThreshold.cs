﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace HFramework.Utils.UI
{
    [RequireComponent(typeof(EventSystem))]
    public class PhysicalDragThreshold : MonoBehaviour
    {
        const float INCH_TO_CM = 2.54f;
        [SerializeField] float dragThresholdCM = 0.5f;

        void OnEnable()
        {
            SetDragThreshold();
        }

        void SetDragThreshold()
        {
            var eventSystem = GetComponent<EventSystem>();
            eventSystem.pixelDragThreshold = (int) (dragThresholdCM * Screen.dpi / INCH_TO_CM);
        }
    }
}