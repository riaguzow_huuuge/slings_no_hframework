﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace HFramework.Utils.UI
{
    [RequireComponent(typeof(Text))]
    public class TimerField : MonoBehaviour
    {
        Text textField;

        Text TextField
        {
            get { return textField != null ? textField : textField = GetComponent<Text>(); }
        }

        public void SetText(string text)
        {
            TextField.text = text;
        }

        public void SetTime(TimeSpan time)
        {
            var timeText = string.Format("{0:00}:{1:00}:{2:00}", time.TotalHours, time.Minutes, time.Seconds);
            SetText(timeText);
        }
    }
}
