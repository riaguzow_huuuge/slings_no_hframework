﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using HFramework.Utils.Extensions;
using HFramework.Utils.PlayerPrefs;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

namespace HFramework.Utils
{
    public class ImageCache
    {
        readonly string url;
        readonly string localURL;
        readonly string path;

        UnityAction<Sprite> OnSuccess { get; set; }
        UnityAction OnError { get; set; }

        Sprite sprite;

        ImageCache(string url, UnityAction<Sprite> OnSuccess, UnityAction OnError)
        {
            this.url = url;
            if (string.IsNullOrEmpty(url))
            {
                Debug.LogError("[Utils] Image url cannot be empty.");
                return;
            }

            path = Utils.GetPath(url);
            localURL = "file://" + path;

            RegisterHandlers(OnSuccess, OnError);

            LoadAndCache();
        }

        void RegisterHandlers(UnityAction<Sprite> OnSuccess, UnityAction OnError)
        {
            this.OnSuccess += OnSuccess;
            this.OnError += OnError;

            if (sprite != null)
                DispatchSuccess();
        }

        void LoadAndCache()
        {
            if (sprite != null)
                DispatchSuccess();
            else if (isCached)
                LoadFromCache();
            else
                Download();
        }

        void Download()
        {
            Debug.LogFormat("[Utils] Downloading texture for url: '{0}'.", url);
            CoroutineManager.StartCoroutine(DownloadCor(url));
        }

        void LoadFromCache()
        {
            Debug.LogFormat("[Utils] Loading texture from path: '{0}'.", localURL);
            CoroutineManager.StartCoroutine(DownloadCor(localURL));
        }

        void SaveToCache(byte[] rawData)
        {
            Debug.LogFormat("[Utils] Saving downloaded texture to cache for url: '{0}' at path: '{1}'.", url, path);
            File.WriteAllBytes(path, rawData);
            CachedPath = path;
        }

        IEnumerator DownloadCor(string url)
        {
            UnityWebRequest request = UnityWebRequestTexture.GetTexture(url);
            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                Debug.LogErrorFormat("[Utils] Error downloading image from: '{0}'. Error: {1}.", url, request.error);
                DispatchError();
            }
            else
            {
                DownloadHandlerTexture download = (DownloadHandlerTexture) request.downloadHandler;
                sprite = Utils.GetSprite(download.texture);

                DispatchSuccess();

                if (!isCached)
                    SaveToCache(download.data);
            }
        }

        void DispatchSuccess()
        {
            OnSuccess.Dispatch(sprite);
            ResetCallbacks();
        }

        void DispatchError()
        {
            OnError.Dispatch();
            ResetCallbacks();
        }

        void ResetCallbacks()
        {
            OnSuccess = null;
            OnError = null;
        }

        bool isCached
        {
            get { return Utils.IsCached(url); }
        }

        string CachedPath
        {
            get { return Utils.GetCachedPath(url); }
            set { Utils.SetCachedPath(url, value); }
        }

        static readonly Dictionary<string, ImageCache> imageCaches = new Dictionary<string, ImageCache>();

        public static void Load(string url, UnityAction<Sprite> OnSuccess, UnityAction OnError = null)
        {
            if (imageCaches.ContainsKey(url))
                imageCaches[url].RegisterHandlers(OnSuccess, OnError);
            else
                imageCaches.Add(url, new ImageCache(url, OnSuccess, OnError));
        }

        static class Utils
        {
            static readonly string directory = Application.persistentDataPath + "/imageCache/";

            static string GenerateName(string url)
            {
                return Random.Range(0, int.MaxValue) + url.Split('/').Last().Split('?')[0];
            }

            internal static string GetPath(string url)
            {
                if (!Directory.Exists(directory))
                    Directory.CreateDirectory(directory);

                if (IsCached(url))
                    return GetCachedPath(url);

                return directory + GenerateName(url);
            }

            internal static bool IsCached(string url)
            {
                return !string.IsNullOrEmpty(GetCachedPath(url));
            }

            internal static string GetCachedPath(string url)
            {
                return HPlayerPrefs.GetString(url, string.Empty);
            }

            internal static void SetCachedPath(string url, string path)
            {
                HPlayerPrefs.SetString(url, path);
            }

            internal static Sprite GetSprite(Texture2D texture)
            {
                Rect rect = new Rect(0, 0, texture.width, texture.height);
                Vector2 pivot = new Vector2(0.5f, 0.5f);
                return Sprite.Create(texture, rect, pivot);
            }
        }
    }
}