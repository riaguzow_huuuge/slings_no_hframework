﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using UnityEditor;
using UnityEngine;

namespace HFramework.Utils.Editor
{
    public class AndroidManifestFixer : AssetPostprocessor
    {
        const string CUSTOM_MANIFEST_PATH = "Assets/Plugins/Android/AndroidManifest.xml";
        const string VUNGLE_MANIFEST_PATH = "Assets/Plugins/Android/Vungle_lib/AndroidManifest.xml";
        const string MOPUB_MANIFEST_PATH = "Assets/Plugins/Android/mopub/AndroidManifest.xml";
        const string MOPUB_SUPPORT_MANIFEST_PATH = "Assets/Plugins/Android/mopub-support/AndroidManifest.xml";

        static string GetCurrentScriptPath()
        {
            var resources = Directory.GetFiles(Application.dataPath, "AndroidManifestFixer.cs", SearchOption.AllDirectories);
            if (resources.Length == 0)
            {
                return string.Empty;
            }
            var path = resources[0].Replace("\\", "/");
            path = path.Substring(path.IndexOf("/Assets/") + 1);
            return path;
        }

        static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            var currentScriptPath = GetCurrentScriptPath();
            var pathsList = Resources.Load<AssetsPathsList>("AssetsPathsList");
            var assetsToObserve = pathsList == null ? new List<string>() : pathsList.Paths;
            var rebuildedManifest = false;
            foreach (var str in importedAssets)
            {
                if (!rebuildedManifest && (str.Equals(currentScriptPath) || assetsToObserve.Contains(str)))
                {
                    RebuildManifests();
                    rebuildedManifest = true;
                }
            }
        }

        [MenuItem("HFramework/Rebuild Android Manifests")]
        static void RebuildManifests()
        {
            var customManifest = LoadAndroidManifest(CUSTOM_MANIFEST_PATH);
            if (customManifest != null)
            {
                AddToolsNamespace(customManifest);
                FixFacebook(customManifest);
                FixThemes(customManifest);
                FixCustomPermissions(customManifest);
                FixCustomFeatures(customManifest);
                SaveFile(customManifest, CUSTOM_MANIFEST_PATH);
                FixAttributeNamespaces(CUSTOM_MANIFEST_PATH);
            }

            var vungleManifest = LoadAndroidManifest(VUNGLE_MANIFEST_PATH);
            if (vungleManifest != null)
            {
                FixVunglePermissions(vungleManifest);
                SaveFile(vungleManifest, VUNGLE_MANIFEST_PATH);
            }

            var mopubManifest = LoadAndroidManifest(MOPUB_MANIFEST_PATH);
            if (mopubManifest != null)
            {
                FixMopubPermissions(mopubManifest);
                SaveFile(mopubManifest, MOPUB_MANIFEST_PATH);
            }
            var mopubSupportManifest = LoadAndroidManifest(MOPUB_SUPPORT_MANIFEST_PATH);
            if (mopubSupportManifest != null)
            {
                FixMopubPermissions(mopubSupportManifest);
                SaveFile(mopubSupportManifest, MOPUB_SUPPORT_MANIFEST_PATH);
            }

            AssetDatabase.SaveAssets();
        }

        static XmlDocument LoadAndroidManifest(string path)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(path);
            } catch { return null; }
            return doc;
        }

        static void AddToolsNamespace(XmlDocument doc)
        {
            var manifestRoot = doc.GetElementsByTagName("manifest").Cast<XmlNode>().First();
            var toolsSchemas = manifestRoot.Attributes.GetNamedItem("xmlns:tools");
            if (toolsSchemas == null || string.IsNullOrEmpty(toolsSchemas.InnerText))
            {
                var toolsUrl =  doc.CreateAttribute("tools");
                toolsUrl.Value = "http://schemas.android.com/tools";
                manifestRoot.Attributes.SetNamedItem(toolsUrl);
            }
        }

        static void FixFacebook(XmlDocument doc)
        {
            var providerName = "com.facebook.FacebookContentProvider";
            var providerNodes = doc.GetElementsByTagName("provider");

            foreach (XmlNode node in providerNodes)
            {
                if (node.Attributes != null && node.Attributes.GetNamedItem("android:name").InnerText == providerName)
                {
                    var auth = node.Attributes.GetNamedItem("android:authorities").InnerText;
                    var id = Regex.Match(auth, @"\d+").Value;
                    var targetValue = providerName + id + PlayerSettings.applicationIdentifier;
                    if (!auth.Equals(targetValue))
                    {
                        node.Attributes.GetNamedItem("android:authorities").InnerText = providerName + id + PlayerSettings.applicationIdentifier;
                        Debug.Log("Facebook provider fixed in AndroidManifest.xml file");
                    }
                    return;
                }
            }
        }

        static void FixThemes(XmlDocument doc)
        {
            var applicationRoot = doc.GetElementsByTagName("application").Cast<XmlNode>().First();
            var themeAttribute = applicationRoot.Attributes.GetNamedItem("android:theme");
            var replaceAttribute = applicationRoot.Attributes.GetNamedItem("tools:replace");
            if (
                (themeAttribute != null && !string.IsNullOrEmpty(themeAttribute.InnerText)) &&
                (replaceAttribute == null || string.IsNullOrEmpty(replaceAttribute.InnerText))
            )
            {
                var newAttribute = doc.CreateNode(XmlNodeType.Attribute, "tools", "replace", "tools");
                newAttribute.Value = "android:theme";
                applicationRoot.Attributes.SetNamedItem(newAttribute);
                Debug.Log("Fix Themes in AndroidManifest.xml file");
            }

        }

        struct ManifestElement
        {
            public string name;
            public bool isToRemove;

            public ManifestElement(string name, bool isToRemove)
            {
                this.name = name;
                this.isToRemove = isToRemove;
            }
        }

        static void FixCustomPermissions(XmlDocument doc)
        {
            var permissionsToAdd = new[]
                { 
                    new ManifestElement("android.permission.INTERNET", false),
                    new ManifestElement("android.permission.ACCESS_NETWORK_STATE", false),
                    new ManifestElement("android.permission.ACCESS_WIFI_STATE", false),
                    new ManifestElement("android.permission.READ_PHONE_STATE", true)
                };

            if (AddElementsToCustomManifest(doc, permissionsToAdd, "uses-permission"))
            {
                Debug.Log("Fix Permissions in Custom AndroidManifest.xml file");
            }
        }

        static void FixCustomFeatures(XmlDocument doc)
        {
            var featuresToAdd = new[]
                { 
                    new ManifestElement("android.software.leanback", true),
                };
            
            if (AddElementsToCustomManifest(doc, featuresToAdd, "uses-feature"))
            {
                Debug.Log("Fix Features in Custom AndroidManifest.xml file");
            }
        }

        static bool AddElementsToCustomManifest(XmlDocument doc, ManifestElement[] elements, string elementsType)
        {
            var manifestRoot = doc.GetElementsByTagName("manifest").Cast<XmlNode>().First();
            var elementsNodes = doc.GetElementsByTagName(elementsType).Cast<XmlNode>();
            var changedPermissions = false;
            foreach (var element in elements)
            {
                var shouldCreateNode = false;
                
                if (!elementsNodes.Any(q => CompareElementName(q, element.name)))
                {
                    shouldCreateNode = true;
                }
                else
                {
                    var node = elementsNodes.First(q => CompareElementName(q, element.name));
                    var hasToolsAttribute =
                        node.Attributes != null && node.Attributes.GetNamedItem("tools:node") != null;
                    if (hasToolsAttribute != element.isToRemove)
                    {
                        var parentNode = node.ParentNode;
                        parentNode.RemoveChild(node);
                    }
                }

                if (shouldCreateNode)
                {
                    var newNode = doc.CreateElement(elementsType);
                    manifestRoot.AppendChild(newNode);

                    var nameAttribute = doc.CreateNode(XmlNodeType.Attribute, "android", "name", "android");
                    nameAttribute.Value = element.name;
                    newNode.Attributes.SetNamedItem(nameAttribute);

                    if (element.isToRemove)
                    {
                        AddToRemoveAttribute(doc, newNode);
                    }
                    changedPermissions = true;
                }
            }
            return changedPermissions;
        }

        static bool CompareElementName(XmlNode node, string nameToCompare)
        {
            if (node.Attributes == null)
                return false;
            var nameItem = node.Attributes.GetNamedItem("android:name");
            return nameItem != null &&
                   node.Attributes.GetNamedItem("android:name").InnerText.Equals(nameToCompare);
        }

        static void AddToRemoveAttribute(XmlDocument doc, XmlNode node)
        {
            var removeAttribute = doc.CreateNode(XmlNodeType.Attribute, "tools", "node", "tools");
            removeAttribute.Value = "remove";
            if (node.Attributes != null) 
                node.Attributes.SetNamedItem(removeAttribute);
        }

        static void FixVunglePermissions(XmlDocument doc)
        {
            var applicationRoot = doc.GetElementsByTagName("application").Cast<XmlNode>().First();
            var changedManifest = false;
            changedManifest |= TryRemoveAttribute(applicationRoot, "android:icon");
            changedManifest |= TryRemoveAttribute(applicationRoot, "android:label");
            var activitiesNodes = doc.GetElementsByTagName("activity");
            foreach (XmlNode activity in activitiesNodes)
            {
                changedManifest |= TryRemoveAttribute(activity, "android:label");
            }

            var permissionsToRemove = new[]
                {
                    "android.permission.INTERNET",
                    "android.permission.ACCESS_NETWORK_STATE",
                    "android.permission.WRITE_EXTERNAL_STORAGE"
                };
            changedManifest |= RemovePermissions(doc, permissionsToRemove);
            if (changedManifest)
            {
                Debug.Log("Fix Permissions in Vungle AndroidManifest.xml file");
            }
        }

        static void FixMopubPermissions(XmlDocument doc)
        {
            var permissionsToRemove = new[]
            {
                "android.permission.ACCESS_FINE_LOCATION",
            };
            if (RemovePermissions(doc, permissionsToRemove))
            {
                Debug.Log("Fix Permissions in Mopub AndroidManifest.xml file");
            }
        }

        static bool RemovePermissions(XmlDocument doc, string[] permissionsToRemove)
        {
            var changedManifest = false;
            var manifestRoot = doc.GetElementsByTagName("manifest").Cast<XmlNode>().First();
            var permissionsNodes = doc.GetElementsByTagName("uses-permission");
            foreach (XmlNode permission in permissionsNodes)
            {
                if (permissionsToRemove.Contains(permission.Attributes.GetNamedItem("android:name").InnerText))
                {
                    manifestRoot.RemoveChild(permission);
                    changedManifest = true;
                }
            }
            return changedManifest;
        }

        static bool TryRemoveAttribute(XmlNode node, string attributeName)
        {
            var attribute = node.Attributes.GetNamedItem(attributeName);
            if (attribute != null && !string.IsNullOrEmpty(attribute.InnerText))
            {
                node.Attributes.RemoveNamedItem(attributeName);
                return true;
            }
            return false;
        }

        static void SaveFile(XmlDocument doc, string path)
        {
            try
            {
                doc.Save(path);
            }
            catch (Exception e)
            {
                Debug.Log("Make sure if AndroidManifest.xml file is not opened by other software \n" + e);
                throw;
            }
        }

        static void FixAttributeNamespaces(string path)
        {
            var manifestContent = File.ReadAllText(path);
            manifestContent = manifestContent.Replace(" xmlns:tools=\"tools\"", string.Empty);
            manifestContent = manifestContent.Replace(" xmlns:android=\"android\"", string.Empty);
            if (!manifestContent.Contains("xmlns:tools=\"http://schemas.android.com/tools\""))
            {
                manifestContent = manifestContent.Replace("tools=\"http://schemas.android.com/tools\"", "xmlns:tools=\"http://schemas.android.com/tools\"");
            }
            File.WriteAllText(path, manifestContent);
        }
    }
}
