﻿namespace HFramework.Utils.Editor
{
    public static class CloudBuildExport
    {
        #if UNITY_CLOUD_BUILD
        public static void PreExport(UnityEngine.CloudBuild.BuildManifestObject manifest)
        {
            #if UNITY_IOS
            if (manifest != null)
            {
                var buildNumber = manifest.GetValue("buildNumber", "unknown");
                UnityEngine.Debug.LogFormat("PreExport: Setup ios build number: {0}", buildNumber);
                UnityEditor.PlayerSettings.iOS.buildNumber = buildNumber;
            }
            else
            {
                UnityEngine.Debug.LogFormat("PreExport: manifest is null");
            }
            #endif
        }
        #endif
    }
}