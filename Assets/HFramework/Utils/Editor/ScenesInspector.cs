﻿using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HFramework.Utils.Editor
{
    public class ScenesInspector : EditorWindow
    {
        const string WINDOW_NAME = "Scenes Inspector";
        const string QUICKPLAY_TEXT = "Play from First Scene";
        
        string ActiveScenePath
        {
            get { return SceneManager.GetActiveScene().path; }
        }

        [MenuItem("HFramework/Windows/"+WINDOW_NAME)]
        static void CreateWindow()
        {
            var window = (ScenesInspector)GetWindow(typeof(ScenesInspector), false, WINDOW_NAME);
            window.Show();
        }

        void OnGUI()
        {
            var defaultColor = GUI.backgroundColor;
            DrawIncludedScenes(defaultColor);
            GUILayout.Space(15);
            DrawPlayButton();
            GUI.backgroundColor = defaultColor;
        }

        void DrawIncludedScenes(Color defaultColor)
        {
            foreach (var scene in EditorBuildSettings.scenes)
            {
                if (scene.path.Equals(ActiveScenePath))
                    GUI.backgroundColor = Color.red;

                if (GUILayout.Button(GetNameFromScenePath(scene.path)))
                {
                    EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
                    EditorSceneManager.OpenScene(scene.path);
                }
                GUI.backgroundColor = defaultColor;
            }
        }

        string GetNameFromScenePath(string path)
        {
            return path.Split('/').Last();
        }

        void DrawPlayButton()
        {
            GUI.backgroundColor = Color.green;
            if (GUILayout.Button(QUICKPLAY_TEXT, GUILayout.Height(40f)))
            {
                EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
                EditorSceneManager.OpenScene(EditorBuildSettings.scenes[0].path);
                EditorApplication.isPlaying = true;
            }
        }
    }
}