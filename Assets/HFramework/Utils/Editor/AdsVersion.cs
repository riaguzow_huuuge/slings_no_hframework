﻿#if ADS && UNITY_ADS_SERVICE
using UnityEditor;
using UnityEngine;
using UnityEngine.Advertisements;

namespace HFramework.Utils.Editor
{
    public class AdsVersion : MonoBehaviour
    {
        [MenuItem("HFramework/UnityAdsVersion")]
        static void ShowAdsVersion()
        {
            Debug.LogFormat("UnityAds Version {0}", Advertisement.version);
        }
    }
}
#endif