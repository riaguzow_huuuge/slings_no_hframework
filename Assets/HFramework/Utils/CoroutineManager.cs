﻿using System.Collections;
using UnityEngine;

namespace HFramework.Utils
{
    public class CoroutineManager : HSingleton<CoroutineManager>
    {
        public new static Coroutine StartCoroutine(IEnumerator routine)
        {
            return ((MonoBehaviour) Instance).StartCoroutine(routine);
        }

        public new static void StopCoroutine(Coroutine routine)
        {
            ((MonoBehaviour) Instance).StopCoroutine(routine);
        }

        void OnDestroy()
        {
            StopAllCoroutines();
        }
    }
}