﻿using System.Collections.Generic;
using UnityEngine;

namespace HFramework.Utils
{
    [CreateAssetMenu(fileName = "AssetsPathsList.asset", menuName = "HFramework/Utils/AssetsPathsList")]
    public class AssetsPathsList : ScriptableObject
    {
        [SerializeField] List<string> paths;
        public List<string> Paths { get { return paths; } }
    }
}

