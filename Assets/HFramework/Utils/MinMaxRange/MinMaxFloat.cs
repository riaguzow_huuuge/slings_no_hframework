﻿using System;
using Random = UnityEngine.Random;

namespace HFramework.Utils.MinMaxRange
{
    [Serializable]
    public class MinMaxFloat : MinMaxRange<float>
    {
        public MinMaxFloat()
        {
        }

        public MinMaxFloat(MinMaxFloat other)
        {
            min = other.min;
            max = other.max;
        }
        
        public MinMaxFloat(float min, float max)
        {
            this.min = min;
            this.max = max;
        }
        
        public override float GetRandomValue()
        {
            return Random.Range(min, max);
        }

        public override string ToString()
        {
            return string.Format("Min: {0:0.00}; Max: {1:0.00};", min, max);
        }
    }
}

