﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace HFramework.Utils
{
    public static class SceneUtils {

        public static bool IsSceneLoaded(string sceneName)
        {
            for (var i = 0; i < SceneManager.sceneCount; i++)
            {
                if (SceneManager.GetSceneAt(i).name == sceneName)
                    return true;
            }

            return false;
        }

        public static void LoadNextScene()
        {
            var scene = SceneManager.GetActiveScene();
            var indexToLoad = Mathf.Clamp(scene.buildIndex + 1, 0, SceneManager.sceneCountInBuildSettings - 1);
            if (indexToLoad >= SceneManager.sceneCountInBuildSettings)
                indexToLoad = SceneManager.sceneCountInBuildSettings;
            SceneManager.LoadScene(indexToLoad);
        }
    }
}
