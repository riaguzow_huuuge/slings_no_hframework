﻿using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.iOS.Xcode;
using UnityEditor.iOS.Xcode.Extensions;
using UnityEngine;

namespace HFramework.Utils.PostBuild.Editor
{
    public class MoPubiOSFrameworkFixer : IPostprocessBuild
    {
        const string pathToFrameworks = "/MoPub/Editor/Support/";

        public int callbackOrder { get; private set; }

        public void OnPostprocessBuild(BuildTarget target, string path)
        {
            if (target != BuildTarget.iOS) return;
                     
            var projectPath = PBXProject.GetPBXProjectPath(path);
            var project = new PBXProject();
            project.ReadFromString(File.ReadAllText(projectPath));
            var targetName = PBXProject.GetUnityTargetName();
            var targetGUID = project.TargetGuidByName(targetName);

            CopyFrameworks(ref project, targetGUID, path);
            AddFrameworks(ref project, targetGUID);
            
            File.WriteAllText(projectPath, project.WriteToString());
        }

        static void CopyFrameworks(ref PBXProject project, string targetGUID, string path)
        {
            var frameworks = new List<string>
            {
                "AppLovinSDK.framework",
                "FBAudienceNetwork.framework",
                "UnityAds.framework"
            };
            foreach (var framework in frameworks)
            {
                CopyAndReplaceDirectory(Application.dataPath + "/MoPub/Editor/Support/" + framework,
                    Path.Combine(path, "Frameworks/" + framework));
                var name = project.AddFile("Frameworks/" + framework, "Frameworks/" + framework,
                    PBXSourceTree.Source);
                Debug.Log("Add File: " + framework);
                project.AddFileToBuild(targetGUID, name);
            }
        }

        static void AddFrameworks(ref PBXProject project, string targetGUID)
        {
            project.AddFrameworkToProject(targetGUID, "AppLovinSDK.framework", false);
            project.AddFrameworkToProject(targetGUID, "FBAudienceNetwork.framework", false);
            project.AddFrameworkToProject(targetGUID, "UnityAds.framework", false);

            project.AddBuildProperty(targetGUID, "OTHER_LDFLAGS", "-ObjC");
            project.AddBuildProperty(targetGUID, "OTHER_LDFLAGS", "-fobjc-arc");
           
        }

        internal static void CopyAndReplaceDirectory(string srcPath, string dstPath)
        {
            if (Directory.Exists(dstPath))
                Directory.Delete(dstPath);
            if (File.Exists(dstPath))
                File.Delete(dstPath);

            Directory.CreateDirectory(dstPath);

            foreach (var file in Directory.GetFiles(srcPath))
                File.Copy(file, Path.Combine(dstPath, Path.GetFileName(file)));

            foreach (var dir in Directory.GetDirectories(srcPath))
                CopyAndReplaceDirectory(dir, Path.Combine(dstPath, Path.GetFileName(dir)));
        }
    }
}