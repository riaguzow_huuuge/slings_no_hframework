﻿using UnityEngine;

namespace HFramework.Utils
{
    public class ConditionalAttribute : PropertyAttribute
    {
        public readonly string conditionProperty;

        public ConditionalAttribute(string conditionProperty)
        {
            this.conditionProperty = conditionProperty;
        }
    }
}