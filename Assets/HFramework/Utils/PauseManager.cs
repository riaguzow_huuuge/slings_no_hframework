﻿using HFramework.Utils.Extensions;
using UnityEngine.Events;

namespace HFramework.Utils
{
    public class PauseManager : HSingleton<PauseManager>
    {
        event UnityAction<bool> onPause;
        public static event UnityAction<bool> OnPause
        {
            add { Instance.onPause += value; }
            remove { Instance.onPause -= value; }
        }
        
        event UnityAction<bool> onFocus;
        public static event UnityAction<bool> OnFocus
        {
            add { Instance.onFocus += value; }
            remove { Instance.onFocus -= value; }
        }

        void OnApplicationPause(bool pauseStatus)
        {
            onPause.Dispatch(pauseStatus);
        }

        void OnApplicationFocus(bool hasFocus)
        {
            onFocus.Dispatch(hasFocus);
        }
    }
}