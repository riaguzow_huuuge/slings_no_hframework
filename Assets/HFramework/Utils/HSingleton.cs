﻿using UnityEngine;

namespace HFramework.Utils
{
    public class HSingleton<T> : MonoBehaviour where T : Component
    {
        static T instance;

        static bool isQuitting;

        void OnApplicationQuit()
        {
            isQuitting = true;
        }

        public static T Instance
        {
            get
            {
                if (instance != null)
                    return instance;

                T[] t = FindObjectsOfType<T>();

                if (t.Length == 1)
                {
                    instance = t[0];
                    return instance;
                }

                if (t.Length > 1)
                {
                    Debug.LogError("More than 1 instance of singleton class!");
                    return t[0];
                }

                if (t.Length == 0 && isQuitting)
                    return null;
                if (t.Length == 0)
                {
                    GameObject go = new GameObject(typeof(T).ToString());
                    instance = go.AddComponent<T>();
                    go.AddComponent<DontDestroyOnLoad>();
                    return instance;
                }

                return null;
            }
        }
    }
}