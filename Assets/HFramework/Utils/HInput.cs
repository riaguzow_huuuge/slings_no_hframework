﻿using UnityEngine;

namespace HFramework.Utils
{
    public static class HInput
    {
        public static bool IsInputPressed()
        {
            #if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            return Input.touches.Length > 0;
            #else
            return Input.GetMouseButton(0);
            #endif
        }

        public static bool IsInputDown()
        {
            #if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            return Input.touches.Length > 0 
                   && Input.touches[0].phase == TouchPhase.Began;      
            #else
            return Input.GetMouseButtonDown(0);
            #endif
        }

        public static Vector2 GetInputPosition()
        {
            #if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            return Input.touches.Length == 0 ? Vector2.zero : Input.touches[0].position;
            #else
            return Input.mousePosition;
            #endif
        }
    }
}