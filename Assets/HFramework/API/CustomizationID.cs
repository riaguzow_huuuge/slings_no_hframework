﻿using System;

namespace HFramework.API
{
    [Serializable]
    public class CustomizationID
    {
        public string categoryID;
        public string itemID;

        public bool Equals(CustomizationID other)
        {
            return categoryID == other.categoryID && itemID == other.itemID;
        }

        public override string ToString()
        {
            return string.Format("Category: {0}; Item: {1}", categoryID, itemID);
        }
    }
}
