﻿using System.Collections;
using HFramework.Audio.API;
using HFramework.Audio.Implementation;
using HFramework.Config;
using HFramework.Development;
using HFramework.Loader;
using HFramework.Utils;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace HFramework.API
{
    public static class HApi
    {
        public static UnityAction<bool> OnGamePause { get; set; }
        public static UnityAction OnGameContinue { get; set; }
        public static UnityAction<CustomizationID> OnCustomizationItemActivated { get; set; }

        private static IEnumerator ReloadGameplay()
        {
            if (SceneUtils.IsSceneLoaded(SceneName.GAMEPLAY))
                yield return SceneManager.UnloadSceneAsync(SceneName.GAMEPLAY);

            if (SceneUtils.IsSceneLoaded(SceneName.GAMEPLAY_UI))
                yield return SceneManager.UnloadSceneAsync(SceneName.GAMEPLAY_UI);

            yield return SceneManager.LoadSceneAsync(SceneName.GAMEPLAY_UI);
            yield return SceneManager.LoadSceneAsync(SceneName.GAMEPLAY, LoadSceneMode.Additive);
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(SceneName.GAMEPLAY));
        }

        public static void StartGame()
        {
            // Do nothing
        }

        public static void LoseGame()
        {
            CoroutineManager.StartCoroutine(ReloadGameplay());
        }

        public static T GetConfig<T>() where T : AbstractConfig
        {
            return configService.GetConfig<T>();
        }

        public static T GetCustomSave<T>() where T : new()
        {
            return new T();
        }

        public static void SetCustomSave<T>(T save)
        {
            // Do nothing
        }

        public static void SetCurrentScore(int score)
        {
            // Do nothing
        }

        public static int GetBestScore()
        {
            return 0;
        }

        public static void AddSoftCurrency(int amount)
        {
        }

        public static bool TryRemoveSoftCurrency(int amount)
        {
            return false;
        }

        public static int GetSoftCurrency()
        {
            return 0;
        }

        public static string GetActiveCustomizationItemID(string category)
        {
            // If item id for this string won't be found 0 will be used as an index for customizations
            return "";
        }


        [PublicAPI]
        public static AudioSource PlaySfx(AudioClip clip)
        {
            return audioPlayer.PlaySfx(clip);
        }

        [PublicAPI]
        public static AudioSource PlayMusic(AudioClip clip)
        {
            return audioPlayer.PlayMusic(clip);
        }

        static IDevPanel devPanel;
        public static IDevPanel DevPanel
        {
            get { return devPanel ?? (devPanel = Object.FindObjectOfType<DevPanel>()); }
        }

        static readonly IConfigService configService;
        static readonly IAudioPlayerModel audioPlayer;

        static HApi()
        {
            configService = new ConfigService();
            audioPlayer = new AudioPlayerModel();
        }
    }
}
