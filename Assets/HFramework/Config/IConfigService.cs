﻿namespace HFramework.Config
{
    public interface IConfigService
    {
        T GetConfig<T>() where T : AbstractConfig;
        T GetConfig<T>(string configID) where T : AbstractConfig;
    }
}