﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace HFramework.Config
{
    [UsedImplicitly]
    public class ConfigService : IConfigService
    {
        Dictionary<Type, List<AbstractConfig>> configMap;

        public ConfigService()
        {
            Debug.Log("[Config] Initializing...");

//TODO: czy tylko z domyslnego folderu Resources?
            BuildConfigMap(Resources.LoadAll<AbstractConfig>(string.Empty));
        }

        public T GetConfig<T>() where T : AbstractConfig
        {
            Type type = typeof(T);

            if (!configMap.ContainsKey(type))
            {
                Debug.LogErrorFormat("Config of type: {0} not found.", type.Name);
                return null;
            }

            return configMap[type][0] as T;
        }

        public T GetConfig<T>(string configID) where T : AbstractConfig
        {
            Type type = typeof(T);

            if (!configMap.ContainsKey(type))
            {
                Debug.LogErrorFormat("Config of type: {0} not found.", type.Name);
                return null;
            }

            T config = configMap[type].Find(x => x.ConfigID == configID) as T;

            if (config == null)
            {
                Debug.LogErrorFormat("Config with configID: {0} not found.", configID);
                return null;
            }

            return config;
        }

        void BuildConfigMap(AbstractConfig[] configs)
        {
            Debug.LogFormat("[Config] Found {0} configs inside 'Resources/' folder.", configs.Length);

            configMap = new Dictionary<Type, List<AbstractConfig>>();

            foreach (AbstractConfig config in configs)
            {
                Type type = config.GetType();

                if (!configMap.ContainsKey(type))
                    configMap[type] = new List<AbstractConfig>();

                configMap[type].Add(config);
            }
        }
    }
}