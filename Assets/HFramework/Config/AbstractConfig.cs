﻿using HFramework.Utils.Extensions;
using UnityEngine;
using UnityEngine.Events;

namespace HFramework.Config
{
    public abstract class AbstractConfig : ScriptableObject
    {
        [SerializeField] bool isRemote; // ignored
        [SerializeField] [ConfigID] protected string configID;

        void OnEnable()
        {
            if (string.IsNullOrEmpty(configID))
                configID = GetType().Name;
        }

        public bool IsRemote
        {
            get { return isRemote; }
        }

        public string ConfigID
        {
            get { return configID; }
        }

        public event UnityAction<AbstractConfig> OnChanged;

        public void ApplyJson(string json)
        {
            if (string.IsNullOrEmpty(json))
                return;

            JsonUtility.FromJsonOverwrite(json, this);
            Debug.LogFormat("[Config] Json applied to config: {0}.", configID);

            OnChanged.Dispatch(this);
        }
    }
}