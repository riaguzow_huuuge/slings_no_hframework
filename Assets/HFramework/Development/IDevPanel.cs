using UnityEngine.Events;

namespace HFramework.Development
{
    public interface IDevPanel
    {
        void AddCustomAction(string name, UnityAction action);
        void RemoveCustomAction(string name);
    }
}