﻿using UnityEngine;

namespace HFramework.Audio.API 
{
    public interface IAudioPlayerModel
    {
        AudioSource PlaySfx(AudioClip clip);
        AudioSource PlaySfx(SfxType sfxType);
        AudioSource PlayMusic(AudioClip clip);
    }
}