﻿namespace HFramework.Audio.API
{
    public enum SfxType
    {
        Other = 0,
        Tap = 1,
        Locked = 2,
        ToggleOn = 3,
        ToggleOff = 4,
        OpenWindow = 5,
        CloseWindow = 6,
        CoinRewardIntro = 7,
        CoinRewardExplode = 8
    }
}