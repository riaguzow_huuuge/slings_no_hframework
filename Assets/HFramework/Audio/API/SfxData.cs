﻿using UnityEngine;

namespace HFramework.Audio.API
{
    [System.Serializable]
    public class SfxData
    {
        public SfxType type = SfxType.Other;
        public AudioClip clip = null;
        [Range(0f, 1f)] public float volume = 1;
        public float delay = 0;
    }
}