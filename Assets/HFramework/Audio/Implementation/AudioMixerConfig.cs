﻿using UnityEngine;
using UnityEngine.Audio;

namespace HFramework.Audio.Implementation
{
    [CreateAssetMenu(fileName = "AudioMixerConfig", menuName = "HFramework/Audio/AudioMixerConfig", order = 1)]
    public class AudioMixerConfig : ScriptableObject
    {
        [SerializeField] float transitionDuration = 0.1f;

        [SerializeField] AudioMixerSnapshot both;
        [SerializeField] AudioMixerSnapshot sfx;
        [SerializeField] AudioMixerSnapshot music;
        [SerializeField] AudioMixerSnapshot none;

        [SerializeField] AudioMixerGroup sfxMixer;
        [SerializeField] AudioMixerGroup musicMixer;

        public AudioMixerGroup SfxMixer
        {
            get { return sfxMixer; }
        }

        public AudioMixerGroup MusicMixer
        {
            get { return musicMixer; }
        }

        public void Refresh(bool sfxEnabled, bool musicEnabled)
        {
            if (sfxEnabled && musicEnabled)
                both.TransitionTo(transitionDuration);
            else if (sfxEnabled)
                sfx.TransitionTo(transitionDuration);
            else if (musicEnabled)
                music.TransitionTo(transitionDuration);
            else
                none.TransitionTo(transitionDuration);
        }
    }
}