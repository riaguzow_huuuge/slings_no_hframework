﻿using System.Collections.Generic;
using System.Linq;
using HFramework.Audio.API;
using JetBrains.Annotations;
using UnityEngine;

namespace HFramework.Audio.Implementation
{
    [UsedImplicitly]
    public class AudioPlayerModel : IAudioPlayerModel
    {
        AudioSource musicSource;
        AudioSource sfxSource;
        float lastTapUpSoundTime;
        readonly SfxClipsConfig sfxClipsConfig;
        readonly List<AudioSource> sfxSources = new List<AudioSource>();

        public AudioPlayerModel()
        {
            sfxClipsConfig = Resources.Load<SfxClipsConfig>("SfxClipsConfig");
        }
        
        AudioSource MusicSource
        {
            get
            {
                if (musicSource == null)
                    musicSource = AudioSourceFactory.CreateMusicAudioSource();
                return musicSource;
            }
        }
        
        AudioSource PlaySfx(SfxData sfxData)
        {
            var audioSource = GetSfxAudioSource();
            audioSource.volume = sfxData.volume;
            audioSource.clip = sfxData.clip;
            audioSource.PlayDelayed(sfxData.delay);

            return audioSource;
        }

        AudioSource IAudioPlayerModel.PlaySfx(AudioClip clip)
        {
            var audioSource = GetSfxAudioSource();
            audioSource.PlayOneShot(clip);
            return audioSource;
        }

        AudioSource IAudioPlayerModel.PlaySfx(SfxType sfxType)
        {
            var sfxInfo = sfxClipsConfig.SfxDictionary[sfxType];
            return PlaySfx(sfxInfo);
        }

        AudioSource IAudioPlayerModel.PlayMusic(AudioClip clip)
        {
            MusicSource.loop = true;
            MusicSource.clip = clip;
            MusicSource.Play();
            return MusicSource;
        }

        AudioSource GetSfxAudioSource()
        {
            AudioSource audioSource = null;
            if (sfxSources.Count > 0)
                audioSource = sfxSources.FirstOrDefault(x => x.isPlaying == false);
            
            if (audioSource == null)
            {
                audioSource = AudioSourceFactory.CreateSfxAudioSource();
                sfxSources.Add(audioSource);
            }

            audioSource.pitch = audioSource.volume = 1f;

            return audioSource;
        }
    }
}