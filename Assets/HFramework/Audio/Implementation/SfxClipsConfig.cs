﻿using System.Collections.Generic;
using HFramework.Audio.API;
using UnityEngine;

namespace HFramework.Audio.Implementation
{    
    [CreateAssetMenu(fileName = "SfxClipsConfig", menuName = "HFramework/Audio/SfxClipsConfig", order = 1)]
    public class SfxClipsConfig : ScriptableObject 
    {
        [SerializeField] List<SfxData> sfxClips;
        
        Dictionary<SfxType, SfxData> sfxDictionary;
        public Dictionary<SfxType, SfxData> SfxDictionary
        {
            get
            {
                if (sfxDictionary == null)
                {
                    sfxDictionary = new Dictionary<SfxType, SfxData>();
                    foreach (var sfxData in sfxClips)
                    {
                        if (sfxDictionary.ContainsKey(sfxData.type))
                            Debug.LogWarningFormat(this, "Sfx: {0} is already loaded", sfxData.type);
                        else
                            sfxDictionary.Add(sfxData.type, sfxData);
                    }
                }
                return sfxDictionary;
            }
        }
    }
}