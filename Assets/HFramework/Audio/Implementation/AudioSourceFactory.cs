﻿using HFramework.Utils;
using UnityEngine;
using UnityEngine.Audio;

namespace HFramework.Audio.Implementation 
{
    public static class AudioSourceFactory
    {
        static Transform audioParent;
        static Transform AudioParent
        {
            get
            {
                if (audioParent == null)
                {
                    var go = new GameObject("Audio");
                    go.AddComponent<DontDestroyOnLoad>();
                    audioParent = go.transform;
                }
                return audioParent;
            }
        }

        static AudioMixerConfig mixerConfig;
        static AudioMixerConfig MixerConfig
        {
            get { return mixerConfig != null ? mixerConfig : mixerConfig = Resources.Load<AudioMixerConfig>("AudioMixerConfig"); }
        }

        public static AudioSource CreateMusicAudioSource()
        {
            return CreateAudioSourceObject("Music", MixerConfig.MusicMixer);
        }

        public static AudioSource CreateSfxAudioSource()
        {
            return CreateAudioSourceObject("SFX", MixerConfig.SfxMixer);
        }
        
        static AudioSource CreateAudioSourceObject(string name, AudioMixerGroup mixerGroup)
        {
            var audioSourceGameObject = new GameObject(name);
            audioSourceGameObject.transform.SetParent(AudioParent);
            var audioSource = audioSourceGameObject.AddComponent<AudioSource>();
            audioSource.outputAudioMixerGroup = mixerGroup;

            return audioSource;
        }
    }
}
