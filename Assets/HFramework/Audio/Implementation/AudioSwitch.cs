using HFramework.Utils.PlayerPrefs;

namespace HFramework.Audio.Implementation
{
    internal class AudioSwitch
    {
        const string MUSIC_PLAYER_PREF = "HFramework.Audio.Music";
        const string SFX_PLAYER_PREF = "HFramework.Audio.Sfx";
        
        bool sfxEnabled;
        bool musicEnabled;

        public AudioSwitch()
        {
            sfxEnabled = HPlayerPrefs.GetBool(SFX_PLAYER_PREF, true);
            musicEnabled = HPlayerPrefs.GetBool(MUSIC_PLAYER_PREF, true);
        }
        
        public bool GetSfxEnabled()
        {
            return sfxEnabled;
        }

        public void SetSfxEnabled(bool value, bool save)
        {
            sfxEnabled = value;
            if (save)
                HPlayerPrefs.SetBool(SFX_PLAYER_PREF, value);
        }

        public bool GetMusicEnabled()
        {
            return musicEnabled;
        }

        public void SetMusicEnabled(bool value, bool save)
        {
            musicEnabled = value;
            if (save)
                HPlayerPrefs.SetBool(MUSIC_PLAYER_PREF, value);
        }
    }
}