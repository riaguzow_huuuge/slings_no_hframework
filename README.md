# What is this

This is based on https://docs.google.com/document/d/1RSBpOfoOIguLIMdkLXbWDpTnNxtbWt4rDjRjSTzMolE/edit#.
Everything which was possible was removed from HFramework to make Kitty Jump work with no Firebase/ads stuff from Huuuge Blast project.

Original Kitty Jump game with HFramework as a submodule is here:
https://bitbucket.org/gamelionstudios/slings/src

HFramework is here:
https://bitbucket.org/gamelionstudios/hframework/src/

It has replaced old Huuge Bomb Framework:
https://bitbucket.org/gamelionstudios/huuugebombframework/src

Games like Keep Turning (https://bitbucket.org/gamelionstudios/zenbox/src) will be ported to new framework.
To use these games in Huuuge Live we needed a dummy version of HFramework which is here.
As for now StrangeIOC was used only in HFramework, not in Kitty Jump game so it wasn't included.
As new games will be ported to HFramework we might need to modify this dummy version.

Just take a directory called HFramework from here and place it instead of original HFramework submodule directory in the Kitty Jump game.

**No changes has been done to Kitty Jump game code.**

# Scenes

Please note that there are 3 scenes (originally there were 4, Core scene from HFramework was removed):

- SlingsSplash (part of the game, load this scene first in UnityEditor)
- GameplayUI (this contains only EventSystem, part of HFramework, in this version it has replaced original scene with UI, now it is empty)
- Gameplay (part of the game)

SlingsSplash replaces itself with GameplayUI and Gameplay additive to GameplayUI.
Reloading game is unloading Gameplay/GameplayUI scenes and loading them once again.
To see example load/unload of the game see HApi.LoseGame().
To see load on game start see LoadHFramework.Start().

Please note that 3 objects left in DontDestroyOnLoad:

- DOTWeen
- Audio
- CoroutineManager

in HApi.LooseGame() are not destroyed when game restarts.
As for now they don't break anything, there are only three of them and Huuuge Live backbone can do it itself if it finds necessary.

Please also note that originally LoadGameplayCommand.cs used AsyncLoaderModel to show a loading circle when game restarted.
This was removed because it would need to bring to many StrangeIOC dependend code like WindowFactory stuff from HFramework.
Loading circle is still present at splash sceen but is not present when Gameplay/GameplayUI scenes are reloaded.
It looks like reloading theese scenes is fast enough to safely remove loading circle in between.

# Content

Only unity-ui-extensions and Utils directories where taken as whole without any changes from HFramework.
First is a separate project from Unity https://bitbucket.org/UnityUIExtensions/unity-ui-extensions/src and second doesn't hurt (some things from these folders are used).

So this repository contains:

- Config directory copied from HFramework to make things work with minor modifications to remove Firebase references
- Audio directory copied from HFramework to make audio work with minor modifications (StrangeIOC commands where removed)
- Utils copied from HFramework which should not hurt and make using other games with this wrapper easier
- unity-ui-extensions copied from HFramework which should not hurt and make using other games with this wrapper easier
- Small dummy files which do nothing in this version but are referenced from inside a game (like a DevPanel) and as such has to be preserved

# Warnings

Please note that when you will start the game there will be bunch of warnings saying:

- The referenced script (Unknown) on this Behaviour is missing!

If you will dock project window somewhere not in the same window as console you will be able to double click on this warnings.
Then ScriptableObject assets like AnalyticsConfig, InterstitialAdsConfig and others in the project window in Kitty Jump game  will be highlighted.
You can then notice their scripts are missing since they were removed from HFramework.
We don't need this configs, so proper solution to these warnings would be to remove these assets from Kitty Jump game.
But current aim was not to modify game to ease further development.
Huuuge Live backbone may choose to remove this assets from Kitty Jump game in the final project.

